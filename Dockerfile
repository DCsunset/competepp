FROM node:latest

WORKDIR /home/node/app

COPY ./backend ./backend

COPY ./backend/node_modules ./backend/node_modules

COPY ./utils ./utils

CMD ["sh","-c","npm i -g ts-node; cd backend; npm run start"]
