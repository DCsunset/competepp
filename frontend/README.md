# frontend

> Frontend

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# if run dev and have errors like tabs, then run below which can fix it
$ npm run lint
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate

```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
