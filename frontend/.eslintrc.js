module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: "babel-eslint"
  },
  extends: ["@nuxtjs", "plugin:nuxt/recommended"],
  // add your custom rules here
  rules: {
    "nuxt/no-cjs-in-config": "off",
    semi: ["warn", "always"],
    "space-before-function-paren": "off",
    "vue/html-self-closing": "off",
    "vue/html-indent": "off",
    "no-tabs": "off",
    indent: "off",
    "vue/attributes-order": "off",
    "import/order": "off",
    "vue/mustache-interpolation-spacing": "off",
    "no-mixed-spaces-and-tabs": "off",
    quotes: "off",
    "arrow-parens": "off",
    "curly": "off",
    "no-unused-vars": "off",
    "require-await": "off"
  }
};
