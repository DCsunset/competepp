import axios from 'axios';
import baseUrl from '@/utils/url.js';

export const state = () => ({
  userData: null
});

export const getters = {
  unreadCount(state) {
    if (!state.userData || !state.userData.notifications)
      return 0;
    return state.userData.notifications.reduce((acc, cur) => acc + (cur.read ? 0 : 1), 0);
  }
};

export const mutations = {
  setUserData(state, data) {
    state.userData = data;
  }
};

export const actions = {
  async fetchUserData(ctx) {
    // Get user data
    const { data } = await axios.get(`${baseUrl}/account`);
    if (data) {
      const { data: res } = await axios.get(`${baseUrl}/users/${data.id}`);
      console.log('User: ', res.data);
      ctx.commit('setUserData', res.data);
    }
  }
};
