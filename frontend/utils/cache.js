import axios from 'axios';
import baseUrl from './url';

const userCache = {};
const competitionCache = {};

// TODO: add timer to clear cache

async function getUser(id, force = false) {
  console.log("get user");
  if (!force && userCache[id])
    return userCache[id];
  const { data } = await axios.get(`${baseUrl}/users/${id}`);
  if (data.success) {
    userCache[id] = data.data;
    return userCache[id];
  } else {
    return null;
  }
}

async function getCompetition(id, force = false) {
  if (!force && competitionCache[id])
    return competitionCache[id];
  const { data } = await axios.get(`${baseUrl}/competitions/${id}`);
  if (data.success) {
    competitionCache[id] = data.competition;
    return competitionCache[id];
  } else {
    return null;
  }
}

export {
  getUser,
  getCompetition
};
