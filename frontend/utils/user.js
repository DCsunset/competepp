export function isOrganizer(userData, organizerId) {
  return userData && userData.status === 1 && userData.group === 'organizer' && userData._id === organizerId;
}

export const USER_STATUS = {
  UNVERIFIED: 0,
  VERIFIED: 1,
  VERIFYING: 2
};
