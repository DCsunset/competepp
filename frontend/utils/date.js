import moment from 'moment';

function formatDate(timestamp) {
  return moment(timestamp).locale('zh_cn').format('LL');
}

export {
  formatDate
};
