import moment from 'moment';

export const COMPETITION_STATUS = {
  VERIFYING: 2,
  CONFIRMING: 3,
  PUBLISHED: 4
};

export const statusChip = (status, enrollDate, competitionDate) => {
  switch (status) {
    case COMPETITION_STATUS.VERIFYING:
      return {
        value: 0,
        text: "审核中",
        color: "orange lighten-3"
      };
    case COMPETITION_STATUS.CONFIRMING:
      return {
        value: 1,
        text: "确认中",
        color: 'orange lighten-3'
      };
    case COMPETITION_STATUS.PUBLISHED:
      const cur = moment();
      if (cur.isBefore(moment(enrollDate.begin), 'day'))
        return {
          value: 2,
          text: "未开始报名",
          color: "grey lighten-1"
        };
      else if (cur.isBefore(moment(enrollDate.end), 'day'))
        return {
          value: 2,
          text: "报名中",
          color: "green lighten-3"
        };
      else if (cur.isBefore(moment(competitionDate.begin), 'day'))
        return {
          value: 3,
          text: "未开始比赛",
          color: "grey lighten-1"
        };
      else if (cur.isBefore(moment(competitionDate.end), 'day'))
        return {
          value: 3,
          text: "进行中",
          color: "blue lighten-3"
        };
      else
        return {
          value: 4,
          text: '已结束'
        };
  }
  return {};
};
