const parse = require('csv-parse/lib/sync')

const fs = require('fs')

const str = fs.readFileSync('test.csv','utf-8')

const records = parse(str, {
	columns: true,
	skip_empty_lines: true,
	delimiter: '\t'
})

console.log(records)
