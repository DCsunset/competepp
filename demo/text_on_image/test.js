const jimp = require('jimp')
const fs = require('fs')

const filename = "certificate.jpg"
const outfile = "out.jpg"
const caption = "caption"

async function main()
{
	const image = await jimp.read(filename)
	const font = await jimp.loadFont(jimp.FONT_SANS_128_BLACK)
	const width = image.bitmap.width
	const height = image.bitmap.height
	print("width")

	const res = await image.print(font,width/3,height/2,"hello world",width, height)
	console.log(typeof(res))
	const buf = res.getBase64Async(jimp.MIME_JPEG)
	await res.write("output.jpg")
}

main()