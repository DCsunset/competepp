const Koa = require('koa')
const koaBody = require('koa-body')
const fs = require('fs')
const app = new Koa()

app.use(koaBody({
	multipart: true
}))

app.use(async (ctx) => {
	const file = ctx.request.files.file
	console.log(file)
	console.log(file.path, file.type, file.name)
	// for (const file of files)
	// {
	// 	const path = file.path
	// 	const type = file.type
	// 	const name = file.name
	// 	console.log(path,type,name)
	// 	// copy from path to local 
	// 	const content = fs.readFileSync(path)
	// 	fs.writeFileSync(name, content)

	// }
	ctx.body = {
		1:1
	}
})

app.listen(3333)
