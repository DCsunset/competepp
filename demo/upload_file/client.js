const file1 = new File(["foo"],"foo.md",{
	type: "text/plain"
})

const file2 = new File(["bar"],"bar.md",{
	type: "text/plain"
})

const data = new FormData()
data.append('file', file1)
//data.append('file',file2)

axios.post("http://localhost:3333", data, 
	{
		headers: {
			'Content-Type': 'multipart/form-data'
		}
	}
).then(res=>{
	console.log(res.data)
})
