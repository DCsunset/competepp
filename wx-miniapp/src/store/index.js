import Vue from 'vue'
import Vuex from 'vuex'
import { statusChip } from './competition.js'
import config from '@/utils/config.js'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    userData: {
      username: ''
    },
    // {
    //   exp: 1578321086,
    //   group: "user",
    //   iat: 1575729086,
    //   id: "5deb83e5871ac5735cffa635",
    //   username: "dddd"
    // },
    competitions: []
    // [
    //   {
    //     cover: "https://img.yzcdn.cn/vant/cat.jpeg",
    //     organizer: "5deb83e5871ac5735cffa633",
    //     status: 4,
    //     title: "qqqq"
    //   },
    //   {
    //     cover: "https://img.yzcdn.cn/vant/cat.jpeg",
    //     organizer: "5deb83e5871ac5735cffa633",
    //     status: 4,
    //     title: "qqqq"
    //   },
    //   {
    //     cover: "https://img.yzcdn.cn/vant/cat.jpeg",
    //     organizer: "5deb83e5871ac5735cffa633",
    //     status: 4,
    //     title: "qqqq"
    //   }
    // ]
  },
  mutations: {
    setUserData (state, data) {
      console.log('store.js setUserData:', data)
      if (!data.avatar) {
        data.avatar = '../../../static/account-box.png'
      } else {
        data.avatar = config.imageUrl + '/pic/' + data.avatar
      }
      state.userData = data
    },
    addCompetition (state, competition) {
      // VERIFYING: 2,
      // CONFIRMING: 3,
      // PUSLISHED: 4
      /* competition.status  */
      const statuschip = statusChip(competition.status, competition.enrollDate, competition.competitionDate)
      competition.status = statuschip.text
      
      /* competition.organizerName */
      if (competition.organizerName && competition.organizerName.length <= 10) {
        competition.organizerName = competition.organizerName.slice(0, 10) + '...'
      }
      if (competition.title && competition.title.length <= 15) {
        competition.title = competition.title.slice(0, 15) + '...'
      }
      
      /* competition.thumbnaol */
      if (competition.thumbnail) {
        competition.thumbnail = config.imageUrl + '/pic/' + competition.thumbnail
      } else {
        competition.thumbnail = config.imageUrl + '/pic/cover/thumbnail.png'
      }
      console.log(competition.title,"", config.imageUrl,":thumbnail", competition.thumbnail)
      state.competitions.push(competition)
    },
    clear (state) {
      state.userData = {
        username: ''
      }
      state.competitions.splice(0, state.competitions.length)
      console.log('clear', state)
    }
  }
})

export default store
