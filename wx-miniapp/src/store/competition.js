import moment from 'moment';

export const COMPETITION_STATUS = {
  VERIFYING: 2,
  CONFIRMING: 3,
  PUSLISHED: 4
};

export const statusChip = (status, enrollDate, competitionDate) => {
  switch (status) {
    case COMPETITION_STATUS.VERIFYING:
      return {
        text: "审核中",
        color: "orange"
      };
    case COMPETITION_STATUS.CONFIRMING:
      return {
        text: "确认中",
        color: 'orange'
      };
    case COMPETITION_STATUS.PUSLISHED:
      const cur = moment();
      if (cur.isBefore(moment(enrollDate.begin), 'day'))
        return {
          text: "尚未开始报名",
          color: "grey"
        };
      else if (cur.isBefore(moment(enrollDate.end), 'day'))
        return {
          text: "报名中",
          color: "green"
        };
      else if (cur.isBefore(moment(competitionDate.begin), 'day'))
        return {
          text: "尚未开始比赛",
          color: "grey"
        };
      else if (cur.isBefore(moment(competitionDate.end), 'day'))
        return {
          text: "进行中",
          color: "primary"
        };
      else
        return {
          text: '已结束',
          disabled: true
        };
  }
  return {}
}
