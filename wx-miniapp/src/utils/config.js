// const host = 'https://2019-a06.iterator-traits.com'
const host = 'http://localhost:2333'
const imageUrl = 'https://2019-a06.iterator-traits.com'
const appid = ''
const appKey = ''
const config = {
  host,
  appid,
  appKey,
  imageUrl
}
export default config
