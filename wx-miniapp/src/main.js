import Vue from 'vue'
import MpvueRouterPatch from 'mpvue-router-patch'
import store from './store/index'
import App from './App'
import 'weapp-cookie'

Vue.use(MpvueRouterPatch)
Vue.prototype.$store = store
Vue.config.productionTip = false
App.mpType = 'app'

const app = new Vue(App)
app.$mount()
//  ***REMOVED***
