import fly from './fly'

import config from '../config'
const host = config.host

/**
 * 接口模版====post
 *
 * export const test = params => {return fly.post(`${root}/xx/xx`, qs.stringify(params))};
 *
 * 接口模版====get
 *
 * export const test1 = function(){return fly.get(`${root}/api/getNewsList`)}
 *
 *
 * 用法：
 * 在 页面用引入 test
 * import {test} from '../../http/api.js'
 *
 * test(params).then(res=>{ console.log(res) })
 */

// 通用的get请求
export const get = (path, params) => {
  console.log('get---ost/path:', host, path, 'params', params)
  return fly.get(`${host}${path}`, params)
}

// 通用的post请求
export const post = (path, params) => {
  console.log('post---ost/path:', host, path, 'params', params)
  return fly.post(`${host}${path}`, params)
}

// 通用的post请求
export const put = (path, params) => {
  console.log('put---ost/path:', host, path, 'params', params)
  return fly.put(`${host}${path}`, params)
}
