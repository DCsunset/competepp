## Docker-compose -> K8s 

1. build your own image and publish it

2. replace the image in docker-compose with your image

3. kompose convert

4. Write Secret for mongo username and password

5. Reference the secret in backend-deployment.yaml and db-deployment.yaml

6. Create database service

7. Setup initContainers for backend pod, waiting for db service to start

8. Create nginx service, type = loadbalancer

9. Create backend and frontend service

10. In nginx, access backend/frontend containerss by their name 

9. Create object
