from locust import HttpLocust, TaskSet, task, between
import json
import secrets

file = open('after-cdn.png', 'rb')


class UserBehavior(TaskSet):
    def on_start(self):
        self.login()

    def login(self):
        self.client.put('/api/account', {
            'username': '软件学院',
            'password': 'rrrrrrrr'
        })

    @task(1)
    def create(self):
        rnd = secrets.randbits(64)
        data = {
            'title': 'locust{}'.format(rnd),
            'description': 'locust',
            'enrollDate': json.dumps({
                'begin': '2019-10-12',
                'end': '2019-11-12'
            }),
            'competitionDate': json.dumps({
                'begin': '2019-12-13',
                'end': '2019-12-20'
            }),
            'options': json.dumps({
                'teamLimit': '3'
            })
        }

        files = [('file', ('after-cdn.png', file, 'image/png'))]
        self.client.post('/api/competitions', data=data, files=files)


class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    wait_time = between(5, 10)
