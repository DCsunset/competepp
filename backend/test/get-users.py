from locust import HttpLocust, TaskSet, task, between
import resource

resource.setrlimit(resource.RLIMIT_NOFILE, (10240, 102400))


class UserBehavior(TaskSet):
    def on_start(self):
        self.login()

    def login(self):
        self.client.put('/api/account', {
            'username': 'aaaa',
            'password': 'aaaaaaaa'
        })

    @task(1)
    def get_users(self):
        self.client.get('/api/users')


class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    wait_time = between(0, 0)
