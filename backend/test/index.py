from locust import HttpLocust, TaskSet, task, between
import resource

resource.setrlimit(resource.RLIMIT_NOFILE, (10240, 102400))


class UserBehavior(TaskSet):
    @task(1)
    def index(self):
        self.client.get('/')

class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    wait_time = between(0, 0)
