# 性能测试


* 测试工具: `locust`
* 测试参数
	* 模拟人数：10000
	* 每秒增长人数：100
	* 两次请求时间间隔: 0s

## 主页

* 测试脚本
	- `index.py`
* 资源占用率
	* 测试前
		- cpu: 4%-10%
		- mem: 3.10G

	* 测试中
		* cpu: 4%-10%
		* mem: 3.09G 
* 并发量
	- 同时在线人数: 10000
	- RPS: 80

很大比例的错误(120/197)是因为网络不稳定(
`Temporary failure in name resolution`),
小部分的错误是因为'Remote end closed connection without response'
可能是因为`CDN`视为`dos`攻击

## 登录

* 测试脚本: `login.py`
* 资源占用率
* 并发量
	- 同时	

## 获取所有比赛

* 测试脚本: `get-competitions.py`
* 资源占用率
	- 测试前
		* cpu=4%-10%
		- mem=2.22G
	- 测试中
		- cpu=4%-10%
		- mem=2.22G

## 获取所有用户

* 测试脚本: `get-users.py`
* 资源占用率
	* 测试前
		- cpu: 4%-10%
		- mem: 2.22G

	* 测试中
		* cpu: 4%-10%
		* mem: 2.22G

* 并发量
	- 同时在线人数: 10000
	- RPS: 401


