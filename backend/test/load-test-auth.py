
from locust import HttpLocust, TaskSet, task, between

class UserBehavior(TaskSet):
    @task(1)
    def getCompetitions(self):
        self.client.get('/api/competitions')

class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    wait_time = between(5, 9)