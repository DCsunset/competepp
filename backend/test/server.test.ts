import { endianness } from "os";

const request = require('supertest')
const {app,server} = require('../server')

// close the server after each test
afterEach(() => {
  server.close()
});

test('server start',async ()=>{
  const response = await request(app.callback()).get("/")
  expect(response.status).toEqual(404)

})

test('register',async ()=>{
  const register = await request(app.callback())
                          .post('/account')
                          .send({
                            username:'linfy',
                            password:'linfy'
                          })
  expect(register.status).toEqual(200)
})

test('compete',async ()=>{
	var token = ""
	var cookie = ""

  const response = await request(app.callback())
                  .post('/account')
                  .send({
                       username:'tai',
                       password:'tai',
                       group:"user"
                  })
  expect(response.status).toEqual(200)            
  console.log(response.data)

  // login
  const res2 = await request(app.callback())
                  .put('/account')
                  .send({
	                  	username:'tai',
	                  	password:'tai'
                  })
  expect(res2.status).toEqual(200)
	// console.log(res2.headers['set-cookie'])
	// cookie = res2.headers['set-cookie'][0]
	// token = cookie.split(";")[0].split("=")[1]

	// create competition
  const res3 = await request(app.callback())
                     .post('/competitions')
                     .set('Cookie',cookie)
                     .send({
		                    	name:'大学生数学建模竞赛',
		                    	description:'这是严肃的描述',
		                    	cover:'https://fake.com',
		                    	certificate:'123'
                     })
})