from locust import HttpLocust, TaskSet, task, between

class UserBehavior(TaskSet):
    def on_start(self):
        self.register()

    def on_stop(self):
        self.logout()

    @task(1)
    def index(self):
        self.client.get("/")

    @task(1)
    def register(self):
        self.client.post('/api/account',{
            'username': '1234',
            'password': '12341234',
            'group': 'user',
            'phone': '15151515155',
            'code': '110123'
        })

    @task(1)
    def login_by_phone(self):
        self.client.put('/api/account',{
            'phone': '15151515155'
        })

    @task(1)
    def login_by_username_password(self):
        self.client.put('/api/acount',{
            'username': '1234',
            'password': '12341234'
        })

    @task(1)
    def login_by_phone_password(self):
        self.client.put('/api/acount',{
            'phone': '15151515155',
            'password': '12341234'
        })

    @task(1)
    def login_by_phone_code(self):
        self.client.put('/api/acount',{
            'phone': '15151515155',
            'code': '123456'
        })

    @task(1)
    def wechat_bind(self):
        self.client.put('/api/acount',{
            'code': '123456',
            'username': '1234',
            'password': '12341234'
        })

    @task(1)
    def login_by_wechat(self):
        self.client.put('/api/acount',{
            'code': '123456'
        })

    @task(1)
    def logout(self):
        self.client.delete('/api/acount')

    @task(1)
    def getCompetition(self):
        self.client.get('/api/competitions/5deb9af10f65a82c02130da5')

    @task(1)
    def addPost(self):
        self.client.post('/api/competitions/123/posts',{
            'value': {
                'title': '123',
                'content': '134'
            }
        })

    @task(1)
    def deletePost(self):
        self.client.patch('/api/competitions/1234/posts',{
            'key': 'delete',
            'value': 13
        })

    @task(1)
    def clickPost(self):
        self.client.patch('/api/competitions/134/posts',{
            'key': 'view',
            'value': 14
        })

    @task(1)
    def commentPost(self):
        self.client.patch('/api/competitions/1234/posts',{
            'key': 'comment',
            'pos': 14,
            'value': 'content'
        })

    @task(1)
    def stickPost(self):
        self.client.patch('/api/competitions/1234/posts',{
            'key': 'sticky',
            'value': {
                'index':1,
                'sticky': True
            }
        })

    @task(1)
    def createCompetition(self):
        self.client.post('/api/competitions',{
            'title': '123' ,
            'status': 1,
            'description': '1231235',
            'enrollDate':
            {
                'begin': '125123516123',
                'end': '125123612'
            },
            'competitionDate':
            {
                'begin': '125123516123',
                'end': '125123612'
            },
            'options': {
                'teamLimit': 4
            },
            'status': 3,
            'certificate': 'http://123.jpg'
        })
    
    @task(1)
    def modifyCompetition(self):
        self.client.put('/api/competitions/1234',{
            'value': {
                'title': '123' ,
                'status': 1,
                'description': '1231235',
                'enrollDate':
                {
                    'begin': '125123516123',
                    'end': '125123612'
                },
                'competitionDate':
                {
                    'begin': '125123516123',
                    'end': '125123612'
                },
                'options': {
                    'teamLimit': 4
                },
                'status': 3,
                'certificate': 'http://123.jpg'
            }
        })

        @task(1)
        def getCompetitionDetail(self):
            self.client.get('/api/competitions/1234')
   
        @task(1)
        def getCompetitionList(self):
            self.client.get('/api/competitions?title=123&organizer=13412341234&description=1283709128570')

        @task(1)
        def publishCompetition(self):
            self.client.patch('/api/competitions/1234',{
                'key': "publish"
            })

        @task(1)
        def uploadCertificateBackground(self):
            self.client.patch('/api/competitions/1234',{
                'key': 'certificate-background',
                'type': 'url',
                'url': 'https://123.jpg'
            })

        @task(1)
        def downloadAward(self):
            self.client.get('/api/competitions/1234/award')

        @task(1)
        def releaseCompetition(self):
            self.client.get('/api/competitions/1234',{
                'key': 'release'
            })

        @task(1)
        def getCertificate(self):
            self.client.get('/api/competitions/1234/certificate/12341234')

        @task(1)
        def verifyCertificate(self):
            self.client.get('/api/competitions/1324/verify?userId=123412341&date=123412341&award=一等奖')

        @task(1)
        def uploadGrade(self):
            self.client.patch('/api/competitions/1234',{
                'key':'grade',
                'value':[]
            })

        @task(1)
        def uploadAward(self):
            self.client.patch('/api/competitions')

        @task(1)
        def deleteCompetition(self):
            self.client.delete('/api/competitions/1234')

        @task(1)
        def verifyCompetition(self):
            self.client.patch('/api/competitions/1234',{
                'key': 'verify',
                'value': True
            })

        # users
        @task(1)
        def getUsers(self):
            self.client.get('/api/users')

        @task(1)
        def getUser(self):
            self.client.get('/api/users/1234')

        @task(1)
        def uploadVerifyDocument(self):
            self.client.patch('/api/users/1234',{
                'key':'verify-document'
            })

        @task(1)
        def veriyXxw(self):
            self.client.patch('/api/users/1234',{
                'key':'verify-xxw',
                'value':'1234'
            })

        @task(1)
        def readNotification(self):
            self.client.patch('/api/users/1234',{
                'key':'read-notification',
                'value':134
            })

        @task(1)
        def deleteUsers(self):
            self.client.delete('/api/users')

        @task(1)
        def verifyUser(self):
            self.client.patch('/api/users/134',{
                'key': 'verify',
                'value': True
            })

        @task(1)
        def getUserDocument(self):
            self.client.get('/api/users/1234/verify-document')

        # teams
        @task(1)
        def createTeam(self):
            self.client.patch('/api/competitions/1234',{
                'key': 'create-team',
                'value':{
                    'name':'team1'
                }
            })

        @task(1)
        def getTeamDetail(self):
            self.client.get('/api/competitions/1234/teams/12341234')

        @task(1)
        def removeMember(self):
            self.client.patch('/api/competitions/1234',{
                'key':'remove-member',
                'value':{
                    'id':'member-id'
                }
            })

        @task(1)
        def joinTeam(self):
            self.client.patch('/api/competitions/1324',{
                'key':'join-team',
                'value':{
                    'name':'member-name'
                }
            })

        @task(1)
        def exitTeam(self):
            self.client.patch('/api/competitions/1234',{
                'key':'exit-team'
            })

        @task(1)
        def disbandTeam(self):
            self.client.patch('/api/competitions/1234',{
                'key':'disband-team'
            })

        @task(1)
        def exitCompetition(self):
            self.client.patch('/api/competitions/1234',{
                'key':'exit-competition'
            })

class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    wait_time = between(5, 9)
