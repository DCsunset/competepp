from locust import HttpLocust, TaskSet, task, between
import resource

resource.setrlimit(resource.RLIMIT_NOFILE, (10240, 102400))


class UserBehavior(TaskSet):
    def on_start(self):
        self.login()

    @task(1)
    def login(self):
        self.client.put('/api/account', {
            'username': 'bbbb',
            'password': 'bbbbbbbb'
        })


class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    wait_time = between(5, 10)
