from locust import HttpLocust, TaskSet, task, between
import resource

resource.setrlimit(resource.RLIMIT_NOFILE, (10240, 102400))


class UserBehavior(TaskSet):
    def on_start(self):
        self.get_competitions()

    @task(1)
    def get_competitions(self):
        self.client.get('/api/competitions')


class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    wait_time = between(0, 0)
