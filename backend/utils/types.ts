export namespace Interfaces{
	export interface Score{
		name: string,
		score: number
	}

	export interface Award{
		name: string,
		award: string
	}
}
