import * as fs from 'fs'
import * as jwt from 'jsonwebtoken'

const config = {
	privateKey: fs.readFileSync('./keys/private.key'),
	publicKey:fs.readFileSync('./keys/public.key'),
	sign_options: <jwt.SignOptions>{
		algorithm:'RS256',
		expiresIn:'30d'
	},
	verify_options:<jwt.VerifyOptions> {
		algorithms:['RS256'],
	}
}

export default config; 