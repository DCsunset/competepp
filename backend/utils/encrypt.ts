import Sha256 = require('crypto-js/sha256')
import Base64 = require('crypto-js/enc-base64')


function sha256(str):string
{
	const hash = Sha256(str);
	return Base64.stringify(hash);
}

export default{
	sha256
}