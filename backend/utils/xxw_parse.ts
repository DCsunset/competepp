const DomParser = require('dom-parser')

function parse(data:string)
{
	try{
		const result = {}
		const parser = new DomParser()
		const dom = parser.parseFromString(data)
		const fixedPart:HTMLElement = dom.getElementById('fixedPart')
		const childs:NodeListOf<ChildNode>= fixedPart.childNodes
		const table1:ChildNode= childs[1]
		const table2:ChildNode = childs[3]

		const gender_and_id:ChildNode = table1.childNodes[3]
		const gender:HTMLElement = <HTMLElement> gender_and_id.childNodes[3].childNodes[0]
		result["gender"] = gender.innerHTML

		const id:HTMLElement = <HTMLElement>gender_and_id.childNodes[7].childNodes[0]
		result["id"] = id.innerHTML

		const nation_and_birth:HTMLElement = <HTMLElement>table1.childNodes[5]
		const nation:HTMLElement = <HTMLElement>nation_and_birth.childNodes[3].childNodes[0]
		result["nation"] = nation.innerHTML
		const birthdate:HTMLElement =<HTMLElement> nation_and_birth.childNodes[7].childNodes[0]
		result["birthdate"] = birthdate.innerHTML

		// school & education
		const school_and_edu = table2.childNodes[1]
		const school:HTMLElement = <HTMLElement>school_and_edu.childNodes[3].childNodes[0]
		result["school"] = school.innerHTML
		const edu:HTMLElement = <HTMLElement>school_and_edu.childNodes[7].childNodes[0]
		result["education"] = edu.innerHTML

		// department & class
		const department_and_class = table2.childNodes[3]
		const department:HTMLElement = <HTMLElement>department_and_class.childNodes[3].childNodes[0]
		result["department"] = department.innerHTML
		const classroom:HTMLElement = <HTMLElement>department_and_class.childNodes[7].childNodes[0]
		console.log("班级=",classroom.innerHTML)
		result["class"] = classroom.innerHTML

		// 专业 & 学号
		const major_and_index = table2.childNodes[5]
		const major:HTMLElement = <HTMLElement>major_and_index.childNodes[3].childNodes[0]
		console.log("专业=",major.innerHTML)
		result["major"] = major.innerHTML
		const index:HTMLElement = <HTMLElement>major_and_index.childNodes[7].childNodes[0]
		result["index"] = index.innerHTML

		// 形式 & 入学时间 & 学制
		let tmp = table2.childNodes[7]
		const form:HTMLElement = <HTMLElement>tmp.childNodes[3].childNodes[0]
		result["form"] = form.innerHTML
		const begin:HTMLElement = <HTMLElement>tmp.childNodes[7].childNodes[0]
		console.log("入学时间=",begin.innerHTML)
		result["begin"] = begin.innerHTML
		const system = <HTMLElement>tmp.childNodes[11].childNodes[0]
		result["school-system"] = system.innerHTML

		// 类型 & 学籍状态
		tmp = <HTMLElement>table2.childNodes[9]
		const type = <HTMLElement>tmp.childNodes[3].childNodes[0]
		result["type"] = type.innerHTML
		
		// console.log("学籍状态=",tmp.childNodes[7].childNodes[0].innerHTML)
		const status = <HTMLElement>tmp.childNodes[7].childNodes[1]
		result["status"] = status.innerHTML.trim()

		console.log("result=",result)
		return result

	}catch(error){
		console.log("error:",error)
		return null
	}
}

export default parse; 