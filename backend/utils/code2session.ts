
import axios from 'axios'

async function code2session(code)
{
	const res = await axios.get("https://api.weixin.qq.com/sns/jscode2session",
	{
		params: {
			"appid": "***REMOVED***",
			"secret": "fefc112909f2cf7204330aafaba35f2f",
			"js_code": code,
			"grant_type": "authorization_code"
		}
	})
	console.log("code2session:",res.data.openid)
	return res.data.openid
}

export default code2session;