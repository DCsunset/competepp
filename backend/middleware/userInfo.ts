import * as Koa from 'koa';
import * as jwt from 'jsonwebtoken';
import jwtConfig from '../utils/jwt-config';

const userInfo = async (ctx: Koa.Context, next: () => Promise<any>) => {
	const token = ctx.cookies.get('token');
	try {
		ctx.user = jwt.verify(token, jwtConfig.publicKey, jwtConfig.verify_options);
	}
	catch {
		ctx.user = null;
	}
	return next();
};

export default userInfo;