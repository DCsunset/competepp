import * as Koa from 'koa';
import db from '../db'

// Check whether the user is admin 
const adminValidate = async (ctx: Koa.Context, next:()=> Promise<any>) => {
	const user = ctx.user
	if (!user)
	{
		ctx.isAdmin = false
		return next()
	}
	else
	{
		const admin = await (await db).getUserbyName(user.username)
		if (admin && admin.group == "admin")
		{
			ctx.isAdmin = true;
		}
		return next()
	}
}

export default adminValidate 
