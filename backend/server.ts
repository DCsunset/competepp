import * as Koa from 'koa';
import * as Router from 'koa-router';
const koaBody = require('koa-body')
// import * as BodyParser from 'koa-bodyparser'
import * as morgan from 'koa-morgan'

import userRouter from './routes/users'
import accountRouter from './routes/account'
import competeRouter from './routes/competitions'

const app: Koa = new Koa()
const router: Router = new Router()

router.use('/users', userRouter.routes())
router.use('/account', accountRouter.routes())
router.use('/competitions', competeRouter.routes())

app.use(async (ctx,next)=>{
	console.log("path=",ctx.path)
	return next()
})

app.use(async (ctx,next)=>{
	const req = <any>ctx.request
	ctx.r = req
	return next()
})


// app.use(BodyParser({}))
app.use(koaBody({
	multipart: true
}))
app.use(router.routes())
app.use(router.allowedMethods())
app.use(morgan('combined', {
	immediate: true
}));

const server = app.listen(2333).on("error", err => {
	console.log(err)
})
console.log("server up")

module.exports = {
	app,
	server
}

