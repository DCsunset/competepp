const Sha256 = require('crypto-js/sha256')
const Base64 = require('crypto-js/enc-base64')

function sha256(str) {
	const hash = Sha256(str);
	return Base64.stringify(hash);
}


console.log(sha256('eeeeeeee'))
