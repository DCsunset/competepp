const host = "140.143.155.232"
const port = 3389
const user = "ubuntu"
const pass = "u_op_Z4afw6Rihp-F09_"
const uuid = require('uuid')

const mongodb = require("mongodb");
const init = async function () {
	console.log("enter async function");
	const url = `mongodb://${user}:${pass}@${host}:${port}/admin?authSource=admin`
	console.log(url)
	const client = await mongodb.MongoClient.connect(
		url,
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	);
	console.log("connected");
	const cps = client.db("competepp").collection("competitions");
	console.log("find collections");
	const userdb = client.db("competepp").collection("users");
	const users = await userdb.find({}).toArray();
	const organizers = await userdb.find({ group: "organizer" }).toArray()

	const mockTeams = [
		{
			id: uuid(),
			name: "Team A",
			leader: users[0]._id,
			members: [users[0]._id, users[1]._id, users[2]._id]
		},
		{
			id: uuid(),
			name: "Team B",
			leader: users[0]._id,
			members: [users[0]._id, users[1]._id, users[2]._id]
		},
		{
			id: uuid(),
			name: "Team C",
			leader: users[0]._id,
			members: [users[0]._id, users[1]._id, users[2]._id]
		},
		{
			id: uuid(),
			name: "Team D",
			leader: users[0]._id,
			members: [users[0]._id, users[1]._id, users[2]._id]
		}
	];

	const mockPosts = [
		{
			title: 'Test Post',
			author: users[0]._id,
			date: '1575251751844',
			content: 'asdf asdf laksdjflkas dflkajsd faaaaaaaaaaaasklfdjlaksfdjll',
			views: 12,
			comments: [
				{
					author: users[0]._id,
					date: '1575251751844',
					content: 'asdf asdf laksdjflkas dflkajsd faaaaaaaaaaaasklfdjlaksfdjll',
				},
				{
					author: users[1]._id,
					date: '1575251751844',
					content: 'asdf asdf laksdjflkas dflkajsd faaaaaaaaaaaasklfdjlaksfdjll',
				}
			]
		}
	];

	await cps.insertMany([
		{
			title: "2019年大数据挑战赛 - 1",
			cover:
				"https://cdn.kesci.com/upload/images/pqgc7j12m.jpeg?imageView2/2/w/400/h/400",
			organizer: organizers[0]._id,
			enrollDate: {
				begin: "2019.10.12",
				end: "2019.11.12"
			},
			options: {
				teamLimit: 4
			},
			teams: [],
			competitionDate: {
				begin: "2019.11.12",
				end: "2019.12.12"
			},
			posts: mockPosts,
			status: 3		//审核完毕
		},
		{
			title: "2019年大数据挑战赛 - 2",
			verified: -1,
			cover:
				"https://cdn.kesci.com/upload/images/pqgc7j12m.jpeg?imageView2/2/w/400/h/400",
			organizer: organizers[1]._id,
			enrollDate: {
				begin: "2019.10.12",
				end: "2019.11.12"
			},
			options: {
				teamLimit: 4
			},
			teams: mockTeams,
			competitionDate: {
				begin: "2019.11.12",
				end: "2019.12.12"
			},
			posts: mockPosts,
			status: 7		//审核不通过或者被手动禁赛
		},
		{
			title: "2019年大数据挑战赛 - 3",
			cover:
				"https://cdn.kesci.com/upload/images/pqgc7j12m.jpeg?imageView2/2/w/400/h/400",
			organizer: organizers[0]._id,
			enrollDate: {
				begin: "2019.10.12",
				end: "2019.11.12"
			},
			competitionDate: {
				begin: "2019.11.12",
				end: "2019.12.12"
			},
			options: {
				teamLimit: 4
			},
			teams: mockTeams,
			posts: mockPosts,
			status: 2	// 待审核
		},
		{
			title: "2019年大数据挑战赛 - 4",
			cover:
				"https://cdn.kesci.com/upload/images/pqgc7j12m.jpeg?imageView2/2/w/400/h/400",
			organizer: organizers[1]._id,
			enrollDate: {
				begin: "2019.10.12",
				end: "2019.11.12"
			},
			competitionDate: {
				begin: "2019.11.12",
				end: "2019.12.12"
			},
			options: {
				teamLimit: 4
			},
			teams: mockTeams,
			posts: mockPosts,
			status: 4		//发布成功开始报名
		},
		{
			title: "2019年大数据挑战赛 - 5",
			cover:
				"https://cdn.kesci.com/upload/images/pqgc7j12m.jpeg?imageView2/2/w/400/h/400",
			organizer: organizers[1]._id,
			enrollDate: {
				begin: "2019.10.12",
				end: "2019.11.12"
			},
			competitionDate: {
				begin: "2019.11.12",
				end: "2019.12.12"
			},
			options: {
				teamLimit: 4
			},
			teams: mockTeams,
			posts: mockPosts,
			status: 5	//比赛进行中
		},
		{
			title: "2019年大数据挑战赛 - 6",
			cover:
				"https://cdn.kesci.com/upload/images/pqgc7j12m.jpeg?imageView2/2/w/400/h/400",
			organizer: organizers[1]._id,
			enrollDate: {
				begin: "2019.10.12",
				end: "2019.11.12"
			},
			teams: mockTeams,
			competitionDate: {
				begin: "2019.11.12",
				end: "2019.12.12"
			},
			posts: mockPosts,
			status: 6	//比赛结束
		}
	]);
	process.exit(0);
};

init();
