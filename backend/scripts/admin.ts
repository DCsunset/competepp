import db from '../db'
import crypto from '../utils/encrypt'
const args = process.argv.slice(2)
const addAdmin = async ()=>
{
	const id:string = args[1]
	const pass:String = args[2]
	const passwordHash:string = crypto.sha256(pass)
	await (await db).addUser({"username":id,passwordHash,"group":"admin",competitions:[]})
	console.log("add admin success")
}

if (args[0] == "create")
{
	addAdmin()
}

else
{
	console.log("Argument error: 3rd parameter should be create")	
}


