const Sha256 = require('crypto-js/sha256')
const Base64 = require('crypto-js/enc-base64')

function sha256(str) {
	const hash = Sha256(str);
	return Base64.stringify(hash);
}

const host = "140.143.155.232"
const port = 3389
const user = "ubuntu"
const pass = "u_op_Z4afw6Rihp-F09_"

const mongodb = require("mongodb");
const init = async function () {
	const url = `mongodb://${user}:${pass}@${host}:${port}/admin?authSource=admin`
	console.log(url)
	const client = await mongodb.MongoClient.connect(
		url,
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	);
	console.log("connected");
	const userdb = client.db("competepp").collection("users");
	await userdb.insertOne(
		{
			username: 'eeee',
			password: sha256('eeeeeeee'),
			group: 'user',
			auth: false,
			bio: 'This is a test',
			competitions: [],
			status: 0
		}
	);
	process.exit(0);
};

init();
