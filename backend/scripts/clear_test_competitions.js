const mongodb = require("mongodb");

const host = "140.143.155.232"
const port = 3389
const user = "ubuntu"
const pass = "u_op_Z4afw6Rihp-F09_"

async function clear_test_data() {
	const client = await mongodb.MongoClient.connect(
		`mongodb://${user}:${pass}@${host}:${port}/admin?authSource=admin`,
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	);
	const competitions = client.db("competepp").collection("competitions");
	const users = client.db("competepp").collection("users");
	await competitions.deleteMany({
		title: {
			$regex: "locust*"
		}
	});
	await users.updateMany({ group: 'admin' }, {
		$set: { notifications: [] }
	});
	console.log("Competition collection cleared successfully");
	process.exit(0);
}

clear_test_data();
