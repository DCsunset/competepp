const host = "140.143.155.232"
const port = 3389
const user = "ubuntu"
const pass = "u_op_Z4afw6Rihp-F09_"

const mongodb = require('mongodb')
const init = async function () {
	const url = `mongodb://${user}:${pass}@${host}:${port}/admin?authSource=admin`
	const client = await mongodb.MongoClient.connect(
		url,
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	);
	const users = client.db("competepp").collection('users')
	const competitions = await client.db('competepp').collection('competitions').find().toArray()

	const notifications = 
				[
					{
						title: "大数据平台赛新消息-1",
						description: "主办方置顶了一条消息",
						read: 0, 
						type: 1,
						competitionId: competitions[0]._id
					},
					{
						title: "大数据平台赛新消息-2",
						description: "主办方置顶了一条消息",
						read: 0, 
						type: 1,
						competitionId: competitions[0]._id
					},
					{
						title: "大数据平台赛新消息-3",
						description: "主办方置顶了一条消息",
						read: 0, 
						type: 1,
						competitionId: competitions[0]._id
					}
				]

	const usernames = ["linfy", "1111", "4444"]
	for (const username of usernames)
	{
		await users.updateOne(
			{username: username},
			{
				$set:
				{
					"notifications": notifications
				}
			}
		)
	}

	process.exit(0)
}

init()