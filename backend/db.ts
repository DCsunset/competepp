import * as mongodb from "mongodb";
import { Interfaces } from './utils/types'
import { createContext } from "vm";
// import * as redis from 'redis'
const redis = require('redis')
const ObjectId = mongodb.ObjectID;
// import bluebird = require('bluebird')
const bluebird = require('bluebird')
bluebird.promisifyAll(redis)

const uuid = require('uuid')
const errorCode = require('../utils/error-code.js')

class Database {
	private uri: string;
	public client: mongodb.MongoClient;
	// public redisClient: redis.RedisClient;
	public useCache: boolean = false;
	public redisClient;

	private async getUserCache()
	{
		const res = await this.redisClient.getAsync('users')
		return JSON.parse(res)
	}

	public constructor(uri: string) {
		this.uri = uri;
	}
	public initRedis() {
		this.redisClient = redis.createClient(6379, 'localhost')
		return this.redisClient
	}

	public async initMongo() {
		this.client = await mongodb.MongoClient.connect(this.uri, {
			useNewUrlParser: true,
			useUnifiedTopology: true
		});
		return this.client
	}

	// Get data from mongodb
	public mgetUsers() {
		const users = this.client.db("competepp").collection("users");
		return users.find().toArray();
	}

	// Get data from redis
	public async getUsers() {
		if (this.useCache) {
			const res = await this.redisClient.getAsync('users')
			if (res) {
				console.log("redis res=", res)
				return JSON.parse(res)
			}
			else {
				const mres: Array<any> = await this.mgetUsers()
				console.log('mres=', mres)
				await this.redisClient.setAsync('users', JSON.stringify(mres))
				return mres
			}
		}
		else {
			return await this.mgetUsers()
		}
	}
	public async getTeamByUser(userid:string, competitionId:string)
	{
		const competitions = this.client.db("competepp").collection("competitions");
		const competition = await competitions.findOne({
			_id: ObjectId.createFromHexString(competitionId)
		}) 
		for (const team of competition.teams)
		{
			if (team.members.map(x=>x.toHexString()).includes(userid))
			{
				return team
			}
		}
		return null
	}
	public async getUserbyName(username: string) {
		const users = this.client.db("competepp").collection("users");
		return users.findOne({ username: username });
	}

	public async getUserbyId(id: string) {
		const users = this.client.db("competepp").collection("users");
		return users.findOne({ _id: ObjectId.createFromHexString(id) });
	}

	public async getUserbyPhone(phone: string) {
		const users = this.client.db("competepp").collection("users");
		return await users.findOne({ phone })
	}
	public async getUserbyOpenid(openid: string) {
		const users = this.client.db("competepp").collection("users");
		return users.findOne({ openid: openid });
	}

	public async addUser(user) {
		const users = this.client.db("competepp").collection("users");
		const insertedId = await users.insertOne(user);
		if (this.useCache)
		{
			let tmp = await this.redisClient.getAsync('users')
			if (tmp)
			{
			let res:Array<any> = JSON.parse(tmp)
			res.push({...user,...{_id: insertedId}})
			await this.redisClient.setAsync('users',JSON.stringify(res))
			console.log("new res=",res)
			}
		}
	}
	public async removeUser(id) {
		const users = this.client.db("competepp").collection("users");
		await users.deleteOne({ _id: ObjectId.createFromHexString(id) });
		if (this.useCache)
		{
			let tmp = await this.redisClient.getAsync('users')
			if (tmp)
			{
			let res:Array<any> = JSON.parse(tmp)
			let newres = []
			for (let i=0;i<res.length;i++)
			{
				if (res[i]._id.toHexString() != id)
				{
					newres.push(res[i])
				}
			}
			await this.redisClient.setAsync('users',JSON.stringify(newres))
			}
		}
	}
	public async updateUser(username, newuser) {
		const users = this.client.db("competepp").collection("users");
		await users.replaceOne({ username }, newuser);
		if (this.useCache)
		{
			let res = await this.redisClient.getAsync('users')
			for (let i=0;i<res.length;i++)
			{
				if (res[i].username == username)
				{
					res[i] = newuser
				}
			}
			await this.redisClient.setAsync('users',JSON.stringify(res))
		}
	}
	public async updateCompetition(id: string, newCompetition) {
		const competitions = this.client.db("competepp").collection("competitions");
		await competitions.replaceOne({ _id: ObjectId.createFromHexString(id) }, newCompetition);
		if (this.useCache)
		{
			let res = await this.redisClient.getAsync('competitions')
			for (let i=0;i<res.length;i++)
			{
				if (res[i]._id.toHexString() == id)
				{
					res[i] = newCompetition
				}
			}
			await this.redisClient.setAsync('competitions',JSON.stringify(res))
		}
	}
	public async updateUserById(id, newuser) {
		const users = this.client.db("competepp").collection("users");
		await users.replaceOne({ _id: ObjectId.createFromHexString(id) }, newuser);
		if (this.useCache)
		{
			let res = await this.redisClient.getAsync('users')
			for (let i=0;i<res.length;i++)
			{
				if (res[i]._id.toHexString() == id)
				{
					res[i] = newuser
				}
			}
			await this.redisClient.setAsync('users',JSON.stringify(res))
		}
	}
	public async updateCompetitionById(id, newcomp) {
		const competitions = this.client.db("competepp").collection("competitions");
		await competitions.replaceOne({ _id: ObjectId.createFromHexString(id) }, newcomp)
		if (this.useCache)
		{
			let res = await this.redisClient.getAsync('competitions')
			for (let i=0;i<res.length;i++)
			{
				if (res[i]._id.toHexString() == id)
				{
					res[i] = newcomp 
				}
			}
			await this.redisClient.setAsync('competitions',JSON.stringify(res))
		}
	}

	public async addCompetition(competition, organizer) {
		const competitions = this.client.db("competepp").collection("competitions");
		// Add competition to competition
		competition["organizer"] = organizer		//username
		competition["status"] = 3	// CONFIRMING
		competition["teams"] = []
		const res = await competitions.insertOne(competition);
		if (this.useCache)
		{
			await this.redisClient.delAsync('competitions')
		}
		const res_id = res.insertedId
		// Add competition to user
		const users = this.client.db("competepp").collection("users");
		await users.updateOne(
			{
				username: organizer
			},
			{
				$addToSet:
				{
					competitions: res_id
				}
			}
		)
		if (this.useCache)
		{
			await this.redisClient.delAsync('competitions')
			await this.redisClient.delAsync('users')
		}
		return res_id
	}

	public async removeCompetition(id) {
		const competitions = this.client.db("competepp").collection("competitions");
		await competitions.deleteOne({ _id: new mongodb.ObjectId(id) })
		if (this.useCache)
		{
			await this.redisClient.delAsync('competitions')
		}
	}

	public async getCompetition(id: string) {
		const competitions = this.client.db("competepp").collection("competitions");
		console.log("competition_id=", id)
		// show all competitions
		const competition = competitions.findOne({ _id: ObjectId.createFromHexString(id) });
		return competition
	}

	public async getTeamByName(competitionId: string, teamname: string) {
		const competitions = this.client.db("competepp").collection("competitions");
		const competition = await competitions.findOne({
			_id: ObjectId.createFromHexString(competitionId)
		})
		const teams = competition.teams
		for (const team of teams) {
			if (team.name == teamname) {
				return team
			}
		}
	}

	public async getTeam(competitionId: string, teamId: string) {
		const competitions = this.client.db("competepp").collection("competitions");
		const competition = await competitions.findOne({
			_id: ObjectId.createFromHexString(competitionId)
		})

		const teams = competition.teams
		for (const team of teams) {
			if (team.id == teamId) {
				return team
			}
		}
	}

	// Check whether the competition is repeatedly created
	public async hasCompetition(title: string) {
		const competitions = this.client.db("competepp").collection('competitions')
		const temp = await competitions.findOne({ title })
		if (!temp) {
			return false;
		}
		else {
			return true
		}
	}

	public async mgetCompetitions() {
		const competitions = this.client.db("competepp").collection("competitions");
		return competitions.find().toArray();
	}

	public async getCompetitions() {
		if (this.useCache) {
			const res = await this.redisClient.getAsync('competitions')
			if (res) {
				console.log("competition redis res=", res)
				return JSON.parse(res)
			}
			else {
				const mres: Array<any> = await this.mgetCompetitions()
				console.log('mres=', mres)
				await this.redisClient.setAsync('competitions', JSON.stringify(mres))
				return mres
			}
		}
		else {
			return await this.mgetCompetitions()
		}
	}


	public async checkAward(competitionId: string, userId: string, award: string) {
		const competitions = this.client.db("competepp").collection("competitions");
		const competition = await competitions.findOne({ _id: ObjectId.createFromHexString(competitionId) })
		const users = this.client.db("competepp").collection("users");
		const user = await users.findOne({ _id: ObjectId.createFromHexString(userId) })
		const username = user.username

		const result = competition.result
		if (result.award) {
			for (const pair of result.award) {
				if (pair.name == username && pair.award == award) {
					return true
				}
			}
		}
		else {
			for (const pair of result.score) {
				if (pair.name == username && pair.score == award) {
					return true
				}
			}
		}
		return false
	}

	public async getPublishedCompetitions() {
		const competitions = this.client.db("competepp").collection("competitions");
		return competitions.find({
			status: 4

		}).toArray()
	}

	public async getResult(competitionId: string) {
		const competitions = this.client.db('competepp').collection('competitions')
		const competition = await competitions.findOne({ _id: ObjectId.createFromHexString(competitionId) })
		return competition.result
	}

	public async uploadAward(value: Array<Interfaces.Award>, competitionId: string) {
		const competitions = this.client.db('competepp').collection('competitions')
		await competitions.updateOne(
			{
				_id: ObjectId.createFromHexString(competitionId),
			},
			{
				$set: {
					result: {
						award: value
					}
				}
			}
		)
		if (this.useCache)
		{
			await this.redisClient.delAsync('competitions')
		}
	}

	public async uploadScore(value: Array<Interfaces.Score>, competitionId: string) {
		const competitions = this.client.db('competepp').collection('competitions')
		await competitions.updateOne(
			{
				_id: ObjectId.createFromHexString(competitionId),
			},
			{
				$set: {
					result: {
						score: value
					}
				}
			}
		)
		if (this.useCache)
		{
			await this.redisClient.delAsync('competitions')
		}
	}

	public async addTeam(username: string, teaminfo, competitionId: string) {
		const competitions = this.client.db("competepp").collection("competitions");

		// Get user id via username
		const users = this.client.db("competepp").collection("users");
		const user = await users.findOne({ username })
		teaminfo["leader"] = ObjectId.createFromHexString(user._id.toHexString())
		teaminfo["id"] = uuid()
		teaminfo["members"] = [teaminfo["leader"]]

		// Insert team into the competition
		await competitions.updateOne(
			{ _id: ObjectId.createFromHexString(competitionId) },
			{
				$addToSet: {
					teams: teaminfo
				}
			}
		);

		// Insert competition into user
		await users.updateOne(
			{ username },
			{
				$addToSet:
				{
					competitions: ObjectId.createFromHexString(competitionId)
				}
			}
		)
		if (this.useCache)
		{
			await this.redisClient.delAsync('users')
			await this.redisClient.delAsync('competitions')
		}
		console.log("insert team into competition")
	}

	public async joinTeam(username: string, teamname: string, competitionId: string) {
		const competitions = this.client.db("competepp").collection("competitions");
		const competition = await competitions.findOne({ _id: ObjectId.createFromHexString(competitionId) })
		const users = this.client.db("competepp").collection("users");
		const user = await (await users).findOne({ username })
		const user_id = user._id.toHexString()
		console.log("user_id=", user_id)
		
		// Check whether verified
		if (parseInt(user.status,10) != 1)
		{
			console.log("not verified user");
			return errorCode.PERMISSION_DENIED;
		}
		console.log("user verified")

		// Check whether has enrolled in the competition
		if (!(user.competitions.map(id=>id.toHexString()).includes(competitionId)))
		{
			console.log("use not enrolled");
			return errorCode.NOT_ENROLLED
		}
		// Check if limitation is exceeded
		const limit = competition.teamLimit
		if (limit) {
			for (const team of competition.teams) {
				if (team.name == teamname) {
					if (team.members && team.members.length + 1 > limit) {
						console.log("exceed limit")
						return errorCode.TEAM_FULL
					}
				}
			}
		}

		// Insert into new team
		console.log("insert into new team")
		await competitions.updateOne(
			{ _id: ObjectId.createFromHexString(competitionId) },
			// Update operator
			{
				$addToSet:
				{
					"teams.$[t].members": ObjectId.createFromHexString(user_id),
				}
			},
			// arrayFilter
			{
				arrayFilters: [
					{
						"t.name": teamname
					}
				]
			}
		)


		// Insert competition into user
		await users.updateOne(
			{ username },
			{
				$addToSet:
				{
					competitions: ObjectId.createFromHexString(competitionId)
				}
			}
		)

		// Remove old single team
		console.log("remove old team")
		console.log("user._id=", user._id, typeof (user._id))
		if (this.useCache)
		{
			await this.redisClient.delAsync('users')
			await this.redisClient.delAsync('competitions')
		}
		// await competitions.updateOne(
		// 	{ _id: ObjectId.createFromHexString(competitionId) },
		// 	{
		// 		$pull:
		// 		{
		// 			'teams':
		// 			{
		// 				leader: ObjectId.createFromHexString(user_id)
		// 			}
		// 		}
		// 	}
		// )

		return 0
	}

	public async removeCompetitionFromUser(username: string, competitionId: string) {
		const users = this.client.db("competepp").collection("users");
		if (this.useCache)
		{
			await this.redisClient.delAsync('users')
		}
		await users.updateOne(
			{ username: username },
			{
				$pull:
				{
					competitions:
					{
						$in: [ObjectId.createFromHexString(competitionId)]
					}
				}
			}
		)
	}
	public async exitTeam(username: string, competitionId: string) {
		const competitions = this.client.db("competepp").collection("competitions");
		const users = this.client.db("competepp").collection("users");
		const user = await (await users).findOne({ username })
		const user_id = user._id.toHexString()
		console.log("Exit team: user_id", user_id)

		// Check if this user is a leader
		let res = await competitions.findOne(
			{ _id: ObjectId.createFromHexString(competitionId) },
		)
		let teams = res.teams
		for (let team of teams) {
			if (team.leader && team.leader.toHexString() == user_id) {
				if (team.members && team.members.length > 1) {
					return false
				}
			}
		}

		if (this.useCache)
		{
			await this.redisClient.delAsync('competitions')
			await this.redisClient.delAsync('users')
		}

		// Remove this user from its team
		
		console.log("old teams = ",res.teams)
		res.teams = teams.filter(team=>{
			team.members = team.members.filter(member => member.toHexString() != user_id)
			return team.members.length > 0;
		})
		console.log("new teams = ", res.teams)
		await this.updateCompetition(competitionId,res)
		// await competitions.updateOne(
		// 	{ _id: ObjectId.createFromHexString(competitionId) },
		// 	{
		// 		$pull:
		// 		{
		// 			'teams.$[t].members': ObjectId.createFromHexString(user_id)
		// 		},
		// 	},
		// 	{
		// 		arrayFilters:
		// 			[
		// 				{
		// 					"t.members":
		// 					{
		// 						$elemMatch: {
		// 							$eq: ObjectId.createFromHexString(user_id)
		// 						}
		// 					}
		// 				}
		// 			]
		// 	}
		// )

		console.log("remove from old team")


		// Remove competition from user
		let user_competitions:Array<any> = user.competitions
		let new_user_competitions = []
		for (const tmp of user_competitions)
		{
			if (tmp && tmp._id && tmp._id.toHexString() == competitionId)
			{
				new_user_competitions.push(tmp)
			}
		}
		let new_user = user
		new_user["competitions"] = new_user_competitions

		await this.updateUser(username, new_user)
		

		// Insert team lead by this user
		// await competitions.updateOne(
		// 	{ _id: ObjectId.createFromHexString(competitionId) },
		// 	{
		// 		$addToSet:
		// 		{
		// 			'teams':
		// 			{
		// 				name: username,
		// 				leader: user._id,
		// 				members: [user._id],
		// 				avatar: ""
		// 			}
		// 		}
		// 	}
		// )
	}

	public async disbandTeam(username: string, competitionId: string) {
		const competitions = this.client.db("competepp").collection("competitions");
		const users = this.client.db("competepp").collection("users");
		const user = await (await users).findOne({ username })

		if (this.useCache)
		{
			await this.redisClient.delAsync('competitions')
		}

		await competitions.updateOne(
			{ _id: ObjectId.createFromHexString(competitionId) },
			{
				$pull:
				{
					'teams':
					{
						leader: { $eq: user._id }
					}
				}
			}
		)
	}
	public async hasMember(username: string, competitionId: string) {
		const competitions = this.client.db("competepp").collection("competitions");
		const competition = await competitions.findOne(
			{
				_id: ObjectId.createFromHexString(competitionId)
			}
		)
		const users = this.client.db("competepp").collection("users");
		const user = await users.findOne({ username })
		const userid = user._id.toHexString()
		const teams = competition.teams
		for (const team of teams) {
			if (team.leader == userid) return true
			for (const memberid of team.members) {
				if (memberid == userid) return true
			}
		}
		return false
	}

	// Check whether username has leadered a team in the competition
	public async hasTeam(username: string, competitionId: string) {
		const competitions = this.client.db("competepp").collection("competitions");
		const users = this.client.db("competepp").collection("users");
		const user = await (await users).findOne({ username })

		const competition = await competitions.findOne({
			_id: ObjectId.createFromHexString(competitionId)
		});
		if (!competition) {
			console.log("no such competition")
			return false;
		}
		const teams = competition.teams;
		console.log("teams=", teams);
		for (const team of teams) {
			if (team.leader == user._id) {
				return true;
			}
		}
		return false;
	}

	public async exitCompetition(username: string, competitionId: string) {
		const competitions = this.client.db("competepp").collection("competitions");
		const users = this.client.db("competepp").collection("users");
		const user = await (await users).findOne({ username })
		const user_id = user._id.toHexString()

		if (this.useCache)
		{
			await this.redisClient.delAsync('competitions')
		}

		await competitions.updateOne(
			{
				_id: ObjectId.createFromHexString(competitionId),
			},
			{
				$pull:
				{
					'teams':
					{
						'leader': ObjectId.createFromHexString(user_id)
					}
				}

			})
	}

	public async getAdmins() {
		const users = this.client.db("competepp").collection("users");
		return await users.find({ group: "admin" }).toArray()
	}

	public async addNotification(userId: string, notification) {
		const users = this.client.db("competepp").collection("users");
		// title, description, type, [competitionId, teamId]
		notification["read"] = 0
		if (notification["competitionId"] && notification["teamId"]) {
			notification["type"] = 2
		}
		else if (notification["competitionId"] && !notification["teamId"]) {
			notification["type"] = 1
		}
		notification["competitionId"] = ObjectId.createFromHexString(notification["competitionId"])
		console.log("addNotification: userid=", userId, "notification=", notification)
		if (this.useCache)
		{
			await this.redisClient.delAsync('users')
		}
		await users.updateOne({
			_id: ObjectId.createFromHexString(userId)
		},
			{
				$addToSet:
				{
					notifications: notification
				}
			})
	}

	public async verifyCompetition(competitionId: string, passed: boolean) {
		const competitions = this.client.db("competepp").collection("competitions");
		if (this.useCache)
		{
			await this.redisClient.delAsync('competitions')
		}

		await competitions.updateOne(
			{
				_id: ObjectId.createFromHexString(competitionId),
			},
			{
				$set:
				{
					"verified": passed ? 1 : -1,
					"status": passed ? 3 : 7
				}
			}
		)
	}

	public async addPost(competitionId: string, post) {
		const competitions = this.client.db("competepp").collection("competitions");
		if (this.useCache)
		{
			await this.redisClient.delAsync('competitions')
		}

		await competitions.updateOne(
			{
				_id: ObjectId.createFromHexString(competitionId),
			},
			{
				$addToSet:
				{
					"posts": post
				}
			}
		)
	}

	public async deletePost(competitionId: string, index: number, userid: string) {
		const competitions = await this.client.db("competepp").collection("competitions");
		let posts: Array<any> = (await competitions.findOne({ _id: ObjectId.createFromHexString(competitionId) })).posts
		if (posts[index].author != userid) {
			console.log(`authorid:${userid} | ${posts[index].author}`)
			return -1
		}
		posts.splice(index, 1)
		if (this.useCache)
		{
			await this.redisClient.delAsync('competitions')
		}

		await this.updatePost(competitionId, posts)
		return 0
	}

	public async commentPost(competitionId: string, index: number, content: string, userid: string) {
		const competitions = await this.client.db("competepp").collection("competitions");
		let posts = (await competitions.findOne({ _id: ObjectId.createFromHexString(competitionId) })).posts
		console.log("index=", index)
		const comment = {
			date: new Date().getTime(),
			content,
			author: ObjectId.createFromHexString(userid)
		}
		posts[index].comments.push(comment)
		if (this.useCache)
		{
			await this.redisClient.delAsync('competitions')
		}
		await this.updatePost(competitionId, posts)
	}

	private async updatePost(competitionId: string, posts: Array<any>) {
		const competitions = await this.client.db("competepp").collection("competitions");
		if (this.useCache)
		{
			await this.redisClient.delAsync('competitions')
		}
		await competitions.updateOne(
			{ _id: ObjectId.createFromHexString(competitionId) },
			{
				$set: {
					"posts": posts
				}
			}
		)
	}

	// Move the indexth post to top
	public async stickPost(competitionId: string, index: number, top: boolean, userid: string) {
		const competitions = await this.client.db("competepp").collection("competitions");
		const users = this.client.db("competepp").collection("users");

		// Check whether user is organizer of competition
		const user = await users.findOne(
			{ _id: ObjectId.createFromHexString(userid) }
		)

		if (user.group != "organizer") {
			return -1
		}
		if (!(user.competitions.map((id: mongodb.ObjectID) => id.toHexString()).includes(competitionId))) {
			return -1
		}
		let posts: Array<any> = (await competitions.findOne({ _id: ObjectId.createFromHexString(competitionId) })).posts
		posts[index].top = top

		if (this.useCache)
		{
			await this.redisClient.delAsync('competitions')
		}
		await this.updatePost(competitionId, posts)
		return 0;
	}

	public async viewPost(competitionId: string, post_index: number) {
		const competitions = this.client.db("competepp").collection("competitions");
		const competition = await competitions.findOne(
			{
				_id: ObjectId.createFromHexString(competitionId)
			}
		)
		const posts = competition.posts
		posts[post_index].views++
		if (this.useCache)
		{
			await this.redisClient.delAsync('competitions')
		}
		await this.updatePost(competitionId, posts)
	}
}

// Test
const host = "140.143.155.232"
const port = 3389
const user = "ubuntu"
const pass = "u_op_Z4afw6Rihp-F09_"

const url = process.env.NODE_ENV === "test" ? 'mongodb://mongo:27017' : `mongodb://${user}:${pass}@${host}:${port}/admin?authSource=admin`;

const database: Database = new Database(url);

export default (async () => {
	await database.initMongo()
	if (database.useCache) {
		await database.initRedis()
	}
	return database
})()
