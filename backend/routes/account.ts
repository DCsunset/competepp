import * as Router from 'koa-router'
import * as jwt from 'jsonwebtoken'
import jwtConfig from '../utils/jwt-config'
import crypto from '../utils/encrypt'
import db from '../db'
import userInfo from '../middleware/userInfo';
import code2session from '../utils/code2session'
const errorCode = require('../../utils/error-code.js')
const QcloudSms = require("qcloudsms_js");
const smsConfig = require('../utils/sms-config.js')

const router: Router = new Router()

let phone2code = {}
let phone2timer = {}
let phone2code_login = {}
let phone2timer_login = {}

router.get('/', userInfo, async ctx => {
	console.log("enter test account")
	ctx.body = ctx.user;
});

router.post('/code', async ctx => {
	const body = ctx.r.body
	const phone: string = body.phone
	console.log("phone=", phone)

	// Generate random code
	const code: string = (Math.floor(100000 + Math.random() * 900000)).toString()
	console.log("verification code=", code)

	// Send code to phone
	const qclousms = QcloudSms(smsConfig.appid, smsConfig.appkey)
	const ssender = qclousms.SmsSingleSender()
	const params = [code, 5]	// Valid in 5 minutes
	ssender.sendWithParam("86", phone, smsConfig.templateId, params, smsConfig.smsSign, "", "", function (err, res, resData) {
		if (err) {
			console.log("err:", err)
		}
		else {
			console.log("request data: ", res.req);
			console.log("response data: ", resData);
			if (phone2timer[phone]) {
				clearTimeout(phone2timer[phone])
			}
			phone2code[phone] = code
			phone2code[phone] = setTimeout(() => {
				if (phone2code[phone]) {
					delete phone2code[phone]
				}
			}, 300000)
		}
	})

	ctx.body = {
		success: true
	}
})

// register:post
router.post('/', async ctx => {
	console.log("enter register")
	const body = ctx.r.body
	const username: string = body.username
	const password: string = body.password
	const group: string = body.group
	const phone: string = body.phone
	const code: string = body.code
	const passwordHash: string = crypto.sha256(password)

	// Check if the user exists
	const user = await (await db).getUserbyName(username)
	if (user) {
		console.log("user exists")
		ctx.body = {
			success: false,
			error_code: errorCode.USER_EXIST,
		}
	}
	else {
		if (phone2code[phone] != code) {
			ctx.body = {
				success: false,
				errorCode: errorCode.WRONG_VERIFICATION_CODE
			}
		}
		delete phone2code[phone]
		console.log("register success")
		// Insert user into database
		await (await db).addUser({ username, password: passwordHash, group, phone, competitions: [], status: 0 })
		ctx.body = {
			success: true,
		}
	}
})

router.put('/code', async ctx => {
	console.log("put code")
	const body = ctx.r.body
	const phone: string = body.phone
	const user = await (await db).getUserbyPhone(phone)
	if (!user) {
		ctx.body = {
			success: false,
			errorCode: errorCode.NO_SUCH_USER
		}
		return
	}

	// Generate random code
	const code: string = (Math.floor(Math.random() * 900000 + 100000)).toString()
	// Send code to phone
	const qclousms = QcloudSms(smsConfig.appid, smsConfig.appkey)
	const ssender = qclousms.SmsSingleSender()
	const params = [code, 5]	// Valid in 5 minutes
	ssender.sendWithParam("86", phone, smsConfig.templateId, params, smsConfig.smsSign, "", "", function (err, res, resData) {
		if (err) {
			console.log("err:", err)
		}
		else {
			if (phone2timer_login[phone]) {
				console.log("clear timeout")
				clearTimeout(phone2timer_login[phone])
			}
			phone2code_login[phone] = code
			console.log("phone2code_login=", phone2code_login)
			phone2timer_login[phone] = setTimeout(() => {
				if (phone2code[phone]) {
					console.log("delete phone2code")
					delete phone2code[phone]
				}
			}, 300000)
		}
	})

	ctx.body = {
		success: true
	}
})


// login:put
// username + password
// phone + password
// code 
// phone + code
// username + password + code
router.put('/', async ctx => {
	console.log("put /account")
	const body = ctx.r.body
	const username = body.username
	const password = body.password
	const code = body.code
	const phone = body.phone
	let user

	// username + password | username + password + code
	if (typeof (username) == "string") {
		const passwordHash: string = crypto.sha256(password)
		user = await (await db).getUserbyName(username)
		if (!user || (passwordHash != user.password)) {
			ctx.body = {
				success: false,
				error_code: errorCode.NO_SUCH_USER
			}
			return
		}
		else {
			// (username,password,code): bind account to openid
			if (typeof (code) == "string") {
				// Remove code from its origin username
				const openid = await code2session(code)
				let olduser = await (await db).getUserbyOpenid(openid)
				if (olduser && olduser.username != user.username) {
					delete olduser.openid
					await (await db).updateUser(olduser.username, olduser)
				}
				// Add code to its new username
				let newuser = user
				newuser["openid"] = openid
				console.log('newuser:', newuser)
				await (await db).updateUser(user.username, newuser)
			}
			// (username, password): do nothing
		}
	}

	// (code)
	else if (typeof (code) == "string" && typeof (phone) != "string") {
		const openid = await code2session(code)
		if (!openid) {
			ctx.body = {
				success: false,
				errorCode: errorCode.WRONG_VERIFICATION_CODE
			}
			return
		}
		user = await (await db).getUserbyOpenid(openid)
		if (!user) {
			ctx.body = {
				success: false,
				errorCode: errorCode.NO_SUCH_USER
			}
			return
		}
	}
	// phone + password | phone + code
	else {
		user = await (await db).getUserbyPhone(phone)
		console.log("user=", user)
		if (!user) {
			console.log("user=null")
			ctx.body = {
				success: false,
				errorCode: errorCode.NO_SUCH_USER
			}
			return
		}
		if (typeof (password) == "string") {
			// phone + password
			const passwordHash: string = crypto.sha256(password)
			if (passwordHash != user.passwordHash) {
				ctx.body = {
					success: false,
					errorCode: errorCode.NO_SUCH_USER
				}
				return
			}
		}
		else {
			// phone + code
			console.log("phone2code_login=", phone2code_login)
			console.log("code=", code)
			if (phone2code_login[phone] != code) {
				ctx.body = {
					success: false,
					errorCode: errorCode.WRONG_VERIFICATION_CODE
				}
				return
			}
			delete phone2code_login[phone]
		}
	}
	console.log("7")
	const result = {
		'username': user.username,
		'group': user.group,
		'id': user._id
	};
	console.log(user._id);
	const token: string = jwt.sign(result, jwtConfig.privateKey, jwtConfig.sign_options)
	ctx.cookies.set('token', token);
	ctx.body = {
		success: true,
	}
})


// logout:delete
router.delete('/', async ctx => {
	if (ctx.cookies.get('token')) {
		ctx.cookies.set('token')
		ctx.body = {
			success: true
		}
	}
	else {
		ctx.body = {
			success: false,
			error_code: errorCode.NO_SUCH_USER
		}
	}
})


export default router;
