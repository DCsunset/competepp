import * as Router from 'koa-router'
import { Interfaces } from '../utils/types'
import db from '../db'
import userInfo from '../middleware/userInfo';
import adminValidate from '../middleware/adminInfo'
const errorCode = require('../../utils/error-code.js')
import * as fs from 'fs'
import request = require('request')
import pathParse = require('path-parse')
import * as jimp from 'jimp'
import * as qrcode from 'qrcode'
import { Jimp } from '@jimp/core';

const sharp = require('sharp')
const TextToSVG = require('text-to-svg');
const textToSVG = TextToSVG.loadSync('SourceHanSerifCN-Regular.otf');

const attributes = { fill: 'black', stroke: 'black' };

const router: Router = new Router()
import { Base64 } from 'js-base64'

router.post('/preview-certificate', async ctx=>{
	const file = ctx.r.files.file
	const fontSize = ctx.r.body.fontSize
	const positions = JSON.parse(ctx.r.body.positions)

	const background = await jimp.read(file.path)

	// qrcode
	const info = {
		name: '123',
		competitionId: '1234',
		award: '1234',
		timestamp: <number>(new Date().getTime())
	}
	let dataurl: string = await qrcode.toDataURL(JSON.stringify(info))
	const end = dataurl.indexOf(',')
	dataurl = dataurl.substring(end + 1)
	const buffer = Buffer.from(dataurl, 'base64')
	let qrcodeimage: Jimp = await jimp.read(buffer)

	if (positions && positions.qrcode)
	{
		await printImage(background,qrcodeimage,positions.qrcode)
	}

	// Team name
	if (positions && positions.team)
	{
		await printText(background,fontSize,'测试队伍名称',positions.team)
	}
	// Teacher
	if (positions && positions.teacher)
	{
		await printText(background,fontSize,"指导教师", positions.teacher)
	}
	// Award
	if (positions && positions.prize)
	{
		await printText(background,fontSize,'一等奖',positions.prize)
	}
	// Members
	if (positions && positions.member)
	{
		const membersPosition = positions.member
		const test_members = ['成员名称']
		for (let i=0;i<test_members.length;i++)
		{
			const y = membersPosition.y + membersPosition.height / test_members.length * i
			const position = {
				x: membersPosition.x,
				y,
				width: membersPosition.width,
				height: membersPosition.height / test_members.length
			}
			await printText(background, fontSize, test_members[i], position)
		}
	}

	// img->base64
	let mime = null
	if (file.name.endsWith(".jpg") || file.name.endsWith(".jpeg")) {
		mime = jimp.MIME_JPEG
	}
	else if (file.name.endsWith(".png")) {
		mime = jimp.MIME_PNG
	}
	const buf: string = await background.getBase64Async(mime)

	ctx.body = {
		success: true,
		image: buf
	}
})

router.get('/:id/verify', async ctx => {
	const query = ctx.r.query
	console.log("query=", query)
	const userId: string = query.userId
	const date = query.date
	const award = Base64.decode(query.award)
	const competitionId: string = ctx.params.id
	console.log(userId, date, award, competitionId)

	const valid = await (await db).checkAward(competitionId, userId, award)
	if (valid) {
		ctx.body = {
			success: true,
			result: {
				userId,
				competitionId,
				date,
				award
			}
		}
	}
	else {
		ctx.body = {
			success: false,
			errorCode: errorCode.PERMISSION_DENIED
		}
	}

})

// Get award list
router.get('/:id/award', async ctx => {
	const competitionId: string = ctx.params.id
	const result = await (await db).getResult(competitionId)
	if (result) {
		console.log("result=", result)
		if (result.award) {
			ctx.body = {
				success: true,
				data: result.award
			}
		}
		else {
			ctx.body = {
				success: true,
				data: result.score
			}
		}
	}
	else {
		ctx.body = {
			success: false,
			errorCode: errorCode.NO_RESULT,
			data: null
		}
	}
})


async function printImage(background: Jimp, foreground, position) {
	let x = position.x * background.bitmap.width
	let y = position.y * background.bitmap.height
	let width = position.width * background.bitmap.width
	let height = position.height * background.bitmap.height
	await foreground.resize(width, height)

	background.composite(foreground, x, y)
}

async function printText(background, fontSize, text, position) {
	console.log("fontsize=",fontSize)
	const x = position.x * background.bitmap.width
	const y = position.y * background.bitmap.height
	const width = position.width * background.bitmap.width
	const height = position.height * background.bitmap.height
	// Text to svg
	const options = { x: 0, y: 0, fontSize, anchor: 'top', attributes: attributes };
	const svg = textToSVG.getSVG(text, options)

	// Svg to png
	fs.writeFileSync('tmp.svg', svg)
	await sharp('tmp.svg').png().toFile('tmp.png')

	const foreground = await jimp.read('tmp.png')
	const offsetx = (width - foreground.bitmap.width) / 2
	const offsety = (height - foreground.bitmap.height) / 2
	background.composite(foreground, x + offsetx, y + offsety)

	// Remove tmp files
	fs.unlinkSync('tmp.png')
	fs.unlinkSync('tmp.svg')
}

// Get award certificate
router.get('/:id/certificate/:userid', userInfo, async ctx => {
	const competitionId: string = ctx.params.id
	const userId: string = ctx.params.userid
	const user = await (await db).getUserbyId(userId)
	const competition = await (await db).getCompetition(competitionId)
	const positions = competition.positions
	const fontSize = parseInt(competition.fontSize)
	console.log("fontsize=", fontSize)

	let prize = competition.result.award
	let isprize = 1
	if (!prize) {
		prize = competition.result.score
		isprize = 0
	}
	console.log("prize=", prize)

	let award = ""
	for (const pair of prize) {
		if (pair["name"] == user.username) {
			if (isprize) {
				award = pair["award"]
			}
			else {
				award = pair["grade"]
			}
		}
	}

	// Check whether the user actually get the award
	if (award == "") {
		ctx.body = {
			success: false,
			errorCode: errorCode.PERMISSION_DENIED
		}
		return
	}

	const dirname = `data/certificate-background/${competitionId}`
	let file = null;
	try {
		file = fs.readdirSync(dirname)[0]
	}
	catch (err) {
		ctx.body = {
			success: false,
			errorCode: errorCode.NO_FILE_OR_DIRECTORY
		}
		return
	}
	const filename = `${dirname}/${file}`
	const background = await jimp.read(filename)
	const date = new Date()

	// print qrcode on background
	const info = {
		name: <string>user.username,
		competitionId: <string>competitionId,
		award: <string>award,
		timestamp: <number>date.getTime()
	}
	let dataurl: string = await qrcode.toDataURL(JSON.stringify(info))
	const end = dataurl.indexOf(',')
	dataurl = dataurl.substring(end + 1)
	const buffer = Buffer.from(dataurl, 'base64')
	let qrcodeimage: Jimp = await jimp.read(buffer)
	console.log("positions=", positions)
	if (positions && positions.qrcode) {
		await printImage(background, qrcodeimage, positions.qrcode)
	}

	// print team, member, prize, teacher on background
	const team = await (await db).getTeamByUser(ctx.user.id, competitionId)
	// Team name
	if (positions && positions.team) {
		await printText(background, fontSize, team.name, positions.team)
	}
	// Teacher
	if (positions && positions.teacher) {
		await printText(background, fontSize, team.teacher, positions.teacher)
	}
	// Award
	if (positions && positions.prize) {
		await printText(background, fontSize, award, positions.prize)
	}
	// Members
	if (positions && positions.member) {
		const membersPosition = positions.member
		for (let i = 0; i < team.members.length; i++) {
			const y = membersPosition.y + membersPosition.height / team.members.length * i
			const position = {
				x: membersPosition.x,
				y,
				width: membersPosition.width,
				height: membersPosition.height / team.members.length
			}
			const memberId = team.members[i].toHexString()
			const membername = await (await db).getUserbyId(memberId)
			await printText(background, fontSize, membername.username, position)
		}
	}

	let mime = null
	if (file.endsWith(".jpg") || file.endsWith(".jpeg")) {
		mime = jimp.MIME_JPEG
	}
	else if (file.endsWith(".png")) {
		mime = jimp.MIME_PNG
	}

	const buf: string = await background.getBase64Async(mime)

	ctx.body = {
		success: true,
		image: buf
	}
})

// Create new post
router.post('/:id/posts', userInfo, async ctx => {
	const value = ctx.r.body.value
	const title: string = value.title
	const authorName = ctx.user.username
	const author = await (await db).getUserbyName(authorName)
	const authorId = author._id.toHexString()
	const content: string = value.content
	const post = {
		title,
		author: authorId,
		content,
		comments: [],
		views: 0,
		date: new Date().getTime()
	}
	await (await db).addPost(ctx.params.id, post)
	ctx.body = {
		success: true
	}
})


// Modify post
router.patch('/:id/posts', userInfo, async ctx => {
	if (!ctx.user) {
		ctx.body = {
			success: false,
			errorCode: errorCode.PERMISSION_DENIED
		}
	}
	const competitionId = ctx.params.id
	const key = ctx.r.body.key
	const value = ctx.r.body.value
	if (key == "view") {
		await (await db).viewPost(competitionId, value)
		ctx.body = {
			success: true
		}
	}
	else if (key == "delete") {
		const res = await (await db).deletePost(competitionId, value, ctx.user.id)
		if (!res) {
			ctx.body = {
				success: true
			}
		}
		else {
			ctx.body = {
				success: false,
				errorCode: errorCode.PERMISSION_DENIED
			}
		}
	}
	else if (key == "comment") {
		const index = ctx.r.body.pos
		const content = ctx.r.body.value
		await (await db).commentPost(competitionId, index, content, ctx.user.id)
		ctx.body = {
			success: true
		}
	}
	else if (key == "sticky") {
		const index: number = ctx.r.body.value.index
		const sticky: boolean = ctx.r.body.value.sticky
		console.log(await (await db).stickPost(competitionId, index, sticky, ctx.user.id))
		ctx.body = {
			success: true
		}
	}
})

// Create new competition
router.post('/', userInfo, async ctx => {
	let body = ctx.r.body
	if (body.options)
		body.options = JSON.parse(body["options"])
	if (body.enrollDate)
		body.enrollDate = JSON.parse(body.enrollDate)
	if (body.competitionDate)
		body.competitionDate = JSON.parse(body.competitionDate)
	if (!ctx.r.files) {
		ctx.body = {
			success: false
		}
		return;
	}
	const file = ctx.r.files.file
	const thumbnail = ctx.r.files.thumbnail

	// Check whether user is organizer
	if (!ctx.user) {
		ctx.body = {
			success: false,
			errorCode: errorCode.PERMISSION_DENIED
		}
	}
	const username = ctx.user.username
	const session = (await db).client.startSession()
	try {
		await session.withTransaction(async () => {
			const temp_user = await (await db).getUserbyName(username)
			if (temp_user.group != "organizer") {
				ctx.body = {
					success: false,
					errorCode: errorCode.PERMISSION_DENIED
				}
			}

			// Check whether the competition has been created 
			const temp = await (await db).hasCompetition(body.title)
			if (temp) {
				ctx.body =
					{
						success: false,
						errorCode: errorCode.DUPLICATE_COMPETITION
					}
				return
			}

			const inserteId = await (await db).addCompetition(body, ctx.user.id);

			// Insert competititon into user database
			const user = await (await db).getUserbyName(ctx.user.username);
			user.competitions.push(inserteId);
			await (await db).updateUser(user.username, user)

			// Add cover and thumbnail to cover/
			fs.mkdirSync(`data/cover/${inserteId.toHexString()}`, { recursive: true })
			if (file !== null && file !== undefined) {
				console.log("file.name=", file.name)
				const tmp = `cover.${file.name.split('.')[file.name.split('.').length - 1]}`
				const filename: string = `data/cover/${inserteId.toHexString()}/${tmp}`
				const content = fs.readFileSync(file.path)
				fs.writeFileSync(filename, content)
			}
			else {
				console.log("cover not uploaded")
			}
			if (thumbnail !== null && thumbnail !== undefined) {
				const tmp1 = `thumbnail.${thumbnail.name.split('.')[thumbnail.name.split('.').length - 1]}`
				const filename1: string = `data/cover/${inserteId.toHexString()}/${tmp1}`
				const content1 = fs.readFileSync(thumbnail.path)
				fs.writeFileSync(filename1, content1)
			}
			else {
				console.log("thumbnail not uploaded")
			}

			const notification = {
				'title': '新比赛',
				'description': `${user.username}创建了比赛:${body.title}`,
				'competitionId': inserteId.toHexString()
			}
			console.log("create new competition: notification={}", notification)
			const admins = await (await db).getAdmins()
			console.log("admins={}", admins)
			for (const admin of admins) {
				await (await db).addNotification(admin._id.toHexString(), notification)
			}
			ctx.body = {
				success: true,
				id: inserteId
			}
		})
	} finally {
		await session.endSession()
	}

});

// Get team detail
router.get('/:id/teams/:teamid', async ctx => {
	const competitionId: string = ctx.params.id
	const teamId: string = ctx.params.teamid
	const team = await (await db).getTeam(competitionId, teamId)
	ctx.body = {
		success: true,
		data: team
	}
})



// Get competition detail
router.get('/:id', userInfo, async ctx => {
	console.log("get competition detail")
	const id: string = ctx.params.id
	let competition = await (await db).getCompetition(id);
	let filename = ""
	let filename1 = ""
	try {
		filename = fs.readdirSync(`./data/cover/${competition._id.toHexString()}`)[0]
		filename1 = fs.readdirSync(`./data/cover/${competition._id.toHexString()}`)[1]
		if (filename.startsWith("thumbnail")) {
			const tmp = filename
			filename = filename1
			filename1 = tmp
		}
	}
	catch (err) {
		console.error(err)
	}
	finally {
		if (filename) {
			console.log("cover filename=", filename)
			competition["cover"] = `cover/${competition._id.toHexString()}/${filename}`
		}
		if (filename1) {
			console.log("thumbnail filename=", filename1)
			competition["thumbnail"] = `cover/${competition._id.toHexString()}/${filename1}`
		}
		if (!ctx.user) {
			ctx.body = {
				success: false,
				errorCode: errorCode.PERMISSION_DENIED
			}
		}
		// If organizer or paricipant, return all
		const a = ctx.user && ctx.user.id == competition.organizer
		const b = ctx.user && await (await db).hasMember(ctx.user.username, ctx.params.id)
		ctx.body = {
			success: true,
			competition
		}
	}
});

// Get competition list
router.get('/', userInfo, adminValidate, async ctx => {
	let competitions: Array<any> = []
	const query = ctx.r.query
	console.log(query)

	// Admin: get all
	competitions = await (await db).getCompetitions()

	// Organizer/user: get verified
	if (!ctx.isAdmin) {
		competitions = await (await db).getPublishedCompetitions()
	}
	if (query && query.title) {
		const title: string = query.title
		const description: string = query.description
		const organizer: string = query.organizer
		const or: boolean = query.or
		console.log(title, description, organizer, or)
		competitions = competitions.filter(competition => {
			const a1: boolean = !title || competition['title'].includes(title)
			const a2: boolean = description == "" || competition['introduction'].includes(description)
			const a3: boolean = !organizer || competition['organizerName'].includes(organizer)
			if (!or && a1 && a2 && a3) {
				return true
			}
			else if (or && (a1 || a2 || a3)) {
				return true
			}
			return false
		})
	}

	competitions.map(competition => {
		let filename = ""
		let filename1 = ""
		try {
			const path = `data/cover/${competition._id.toHexString()}/`
			filename = fs.readdirSync(path)[0]
			filename1 = fs.readdirSync(path)[1]
			if (filename.startsWith("thumbnail")) {
				const tmp = filename
				filename = filename1
				filename1 = tmp
			}
		}
		catch (err) {
			filename = ""
			filename1 = ""
		}
		finally {
			if (filename !== "") {
				competition["cover"] = `cover/${competition._id.toHexString()}/${filename}`
			}
			else {
				competition["cover"] = ""
			}
			if (filename1 !== "") {
				competition["thumbnail"] = `cover/${competition._id.toHexString()}/${filename1}`
			}
			else {
				competition["thumbnail"] = ""
			}
			return competition
		}
	})

	ctx.body = {
		success: true,
		competitions
	}
});


// Delete competition: only permitted to admin
router.delete('/:id', userInfo, adminValidate, async ctx => {
	console.log("delete competition")
	const competitionId: string = ctx.params.id
	if (ctx.isAdmin) {
		await (await db).removeCompetition(competitionId)
		await (await db).removeCompetitionFromUser(ctx.user.username, competitionId)
		ctx.body = {
			success: true
		}
	}
	else {
		const competition = await (await db).getCompetition(competitionId)
		await (await db).removeCompetitionFromUser(ctx.user.username, competitionId)
		console.log("ctx.user.id=", ctx.user.id)
		console.log("competition.organizer=", competition.organizer)
		if (ctx.user.id != competition.organizer) {
			ctx.body = {
				success: false,
				errorCode: errorCode.PERMISSION_DENIED
			}
			return
		}
		await (await db).removeCompetition(competitionId)
		ctx.body =
			{
				success: true,
			}

	}
})

router.patch('/:id', userInfo, adminValidate, async ctx => {
	const body = ctx.r.body
	console.log("upload competition result:body=", body)
	const key: string = body.key			//"result"
	const id: string = ctx.params.id
	const username = ctx.user.username

	// Upload competition result
	if (key == "grade") {
		const value = body.value		//[{name:"aaa",grade:1234}]
		console.log("grade:value=", value)
		await (await db).uploadScore(value, id)
		ctx.body = {
			success: true
		}
	}
	else if (key == "remove-member") {
		console.log("remove member")
		const value = body.value
		const memberid: string = value.id  	//userid 
		// Remove the user out of the team
		let competition = await (await db).getCompetition(id)
		const teams = competition.teams
		let team = null
		for (let i = 0; i < teams.length; i++) {
			if (teams[i].leader.toHexString() == ctx.user.id) {
				console.log("removed from team:", teams[i])
				team = teams[i]
				let newmembers = []
				for (const member of competition.teams[i].members) {
					if (member.toHexString() != memberid) {
						newmembers.push(member)
					}
				}
				console.log("new member=", newmembers)
				competition.teams[i].members = newmembers
				break
			}
		}
		await (await db).updateCompetitionById(id, competition)
		const notification = {
			title: '加入队伍被拒绝',
			description: `您参加的比赛${competition.title}中的队伍${team.name}拒绝了您的加入请求`,
			competitionId: id,
			teamId: team.id
		}
		console.log("remove member, notification = ", notification, "to:", memberid)
		await (await db).addNotification(memberid, notification)
		ctx.body = {
			success: true
		}
	}
	else if (key == "publish") {
		console.log("publish")
		let competition = await (await db).getCompetition(id)
		competition.status = 4
		await (await db).updateCompetition(id, competition)
		ctx.body = {
			success: true
		}
	}
	else if (key == "award") {
		const value = body.value
		console.log("award.value=", value)
		await (await db).uploadAward(value, id)
		ctx.body = {
			success: true
		}
	}
	else if (key == "certificate-background") {
		const type: string = ctx.r.body.type
		const positions = ctx.r.body.positions
		let competition = await (await db).getCompetition(id)
		competition["positions"] = JSON.parse(positions)
		competition["fontSize"] = ctx.r.body.fontSize
		await (await db).updateCompetitionById(id, competition)

		if (type == "file") {
			const file = ctx.r.files.file
			// Copy from path to local
			const tmp = `background.${file.name.split('.')[file.name.split('.').length - 1]}`
			const filename: string = `data/certificate-background/${ctx.params.id}/${tmp}`
			const content = fs.readFileSync(file.path)
			fs.mkdirSync(`data/certificate-background/${ctx.params.id}`, { recursive: true })
			fs.writeFileSync(filename, content)
		}
		else if (type == "url") {
			const url: string = ctx.r.files.url
			console.log("url=", url)
			const name = pathParse(url).base
			const filename: string = `data/certificate-background/${ctx.params.id}/${name}`
			request({ url, encoding: null }, function (err, res, body) {
				fs.writeFileSync(filename, body)
			})
		}
		ctx.body = {
			success: true
		}
	}
	// Create team
	else if (key == "create-team") {
		const value = body.value
		// Check if the leader has been in a team in this competition 
		const duplicate = await (await db).hasTeam(username, id)
		if (duplicate) {
			ctx.body = {
				success: false,
				errorCode: errorCode.DUPLICATE_TEAM
			}
		}
		else {
			await (await db).addTeam(username, value, id)
			ctx.body = {
				success: true
			}
		}
	}
	else if (key == "join-team") {
		const teamname: string = body.value.name
		const username: string = ctx.user.username
		console.log("teamname=", teamname, "username=", username)
		const res = await (await db).joinTeam(username, teamname, id)


		if (res) {
			console.log("join team fail")
			ctx.body = {
				success: false,
				errorCode: errorCode.TEAM_FULL
			}
		}
		else {
			console.log("join team success")

			// Notify leader
			const team = await (await db).getTeamByName(id, teamname)
			const notification = {
				"title": "新队员",
				"description": `您的队伍${teamname}有新成员${username}加入`,
				"competitionId": id,
				"teamId": team.id
			}
			console.log(`join team: notification=${notification}`)

		await (await db).addNotification(team.leader.toHexString(), notification)
			ctx.body = {
				success: true
			}
		}
	}

	else if (key == "exit-team") {
		const username: string = ctx.user.username
		const res = await (await db).exitTeam(username, id)
		if (res)
		{
			ctx.body = {
				success: true
			}
		}
		else
		{
			ctx.body = {
				success: false
			}
		}
	}

	else if (key == "disband-team") {
		const username: string = ctx.user.username
		await (await db).disbandTeam(username, id)
		ctx.body = {
			success: true
		}
	}

	else if (key == "exit-competition") {
		const username: string = ctx.user.username
		await (await db).exitCompetition(username, id)
		ctx.body = {
			success: true
		}
	}
	else if (key == "introduction") {
		const user = await (await db).getUserbyId(ctx.user.id)
		if (user.group !== 'organizer') {
			ctx.body = {
				success: false,
				errorCode: errorCode.PERMISSION_DENIED
			}
			return
		}
		const introduction = ctx.r.body.value
		const id = ctx.params.id
		let competition = await (await db).getCompetition(id)
		competition["introduction"] = introduction
		await (await db).updateCompetitionById(id, competition)
		ctx.body = {
			success: true
		}

	}
	else if (key == "problem") {
		const user = await (await db).getUserbyId(ctx.user.id)
		if (user.group !== 'organizer') {
			ctx.body = {
				success: false,
				errorCode: errorCode.PERMISSION_DENIED
			}
			return
		}
		const problem = ctx.r.body.value
		const id = ctx.params.id
		let competition = await (await db).getCompetition(id)
		competition["problem"] = problem
		await (await db).updateCompetitionById(id, competition)
		ctx.body = {
			success: true
		}
	}
	else if (key == "verify") {
		if (!ctx.isAdmin) {
			console.log("only admin can verify")
			ctx.body = {
				success: false,
				errorCode: errorCode.PERMISSION_DENIED
			}
		}
		const pass: boolean = (body.value)
		await (await db).verifyCompetition(id, pass)
		const competition = await (await db).getCompetition(id)
		let notification = null
		if (pass) {
			notification = {
				"title": "比赛通过审核",
				"description": `您的比赛${competition.title}已经通过审核`,
				'competitionId': id
			}
		}
		else {
			notification = {
				'title': '比赛不通过审核',
				"description": `您的比赛${competition.title}没有通过审核`,
				'competitionId': id
			}
		}
		await (await db).addNotification(competition.organizer, notification)
		console.log("verified")
		ctx.body = {
			success: true
		}
	}
})

// Modify competition info
router.put('/:id', userInfo, async ctx => {
	console.log("Modify competition result")
	const user = ctx.user
	const value = ctx.r.body
	value.options = JSON.parse(value["options"])
	value.enrollDate = JSON.parse(value.enrollDate)
	value.competitionDate = JSON.parse(value.competitionDate)
	if (value._id)
		delete value._id;
	if (user.id != value.organizer) {
		console.log(`user.id=${user.id}, competition.organizer=${value.organizer}`)
		ctx.body = {
			success: false,
			errorCode: errorCode.PERMISSION_DENIED
		}
		return
	}
	if (value.file)
		delete value.file;
	await (await db).updateCompetition(ctx.params.id, value)

	// Update thumbnail/cover 
	const files = ctx.r.files
	if (files && files.file) {
		const file = files.file
		const tmp = `cover.${file.name.split('.')[file.name.split('.').length - 1]}`
		const filename: string = `data/cover/${ctx.params.id}/${tmp}`
		const content = fs.readFileSync(file.path)
		fs.mkdirSync(`data/cover/${ctx.params.id}`, { recursive: true })
		fs.writeFileSync(filename, content)
	}

	if (files && files.thumbnail) {
		const file = files.thumbnail
		const tmp = `thumbnail.${file.name.split('.')[file.name.split('.').length - 1]}`
		const filename: string = `data/cover/${ctx.params.id}/${tmp}`
		const content = fs.readFileSync(file.path)
		fs.mkdirSync(`data/cover/${ctx.params.id}`, { recursive: true })
		fs.writeFileSync(filename, content)
	}

	ctx.body = {
		success: true
	}
})

export default router;



