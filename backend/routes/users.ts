import * as Router from "koa-router";
import db from "../db";
const axios = require('axios')
import * as _ from "lodash";
// import errorCode from "../../utils/error-code";
const errorCode = require('../../utils/error-code.js')
import userInfo from '../middleware/userInfo';
import adminValidate from '../middleware/adminInfo'
import xxwParse from '../utils/xxw_parse'
import { Base64 } from 'js-base64'
import * as fs from 'fs'
import * as path from 'path'

const router: Router = new Router();

// Get all users: only permitted to admin 
router.get('/', userInfo, adminValidate, async ctx => {
	if (ctx.isAdmin == true) {
		ctx.body = {
			success: true,
			users: await (await db).getUsers()
		}
	}
	else {
		ctx.body = {
			success: false,
			errorCode: errorCode.PERMISSION_DENIED
		}
	}
})


// Get user verify-document, only permitted to admin
router.get('/:id/verify-document', userInfo, adminValidate, async ctx => {
	if (!ctx.isAdmin) {
		ctx.body = {
			success: false,
			errorCode: errorCode.PERMISSION_DENIED
		}
	}

	const id: string = ctx.params.id
	const dirname: string = `data/verify-document/${id}/`
	const filenames: Array<string> = fs.readdirSync(dirname)
	const result: Array<Object> = []		// type: base64-content
	for (let i = 0; i < filenames.length; i++) {
		const filename: string = filenames[i]
		const file = fs.readFileSync(`${dirname}/${filename}`)
		const code = new Buffer(file).toString('base64');
		result.push({
			type: path.extname(filename),
			content: code
		})
	}
	ctx.body = {
		success: true,
		file: result
	}
})

// Get detailed user info
router.get("/:id", userInfo, async ctx => {
	const id: string = ctx.params.id;
	let data = await (await db).getUserbyId(id);
	if (!data) {
		ctx.body = {
			success: false,
			errorCode: errorCode.NO_SUCH_USER
		};
		return;
	}
	if (!ctx.user || ctx.user.id !== id) {
		delete data.notifications;
	}
	data = _.pick(data, ["_id", "username", "group", "bio", "status", "competitions", "notifications"])
	const path = `data/avatar/${ctx.user.id}`
	let filename = []
	try {
		filename = fs.readdirSync(path)
	}
	catch
	{
		ctx.body = {
			success: false
		}
	}
	finally {
		console.log("filename=", filename)
		if (filename && filename.length > 0) {
			console.log("1")
			data["avatar"] = `/pic/avatar/${ctx.user.id}/${filename[0]}`
		}
		else {
			data["avatar"] = ""
		}
		ctx.body = {
			success: true,
			data
		}

	}
});

router.patch("/:id", userInfo, adminValidate, async ctx => {
	// if (!ctx.user || ctx.user.id !== ctx.params.id) {
	// 	ctx.body = {
	// 		success: false,
	// 		errorCode: errorCode.PERMISSION_DENIED
	// 	};
	// 	return;
	// }

	// const validKeys = new Set(["bio", "password", "verify-xxw", "verify", "verify-document", 'read-notification']);

	// // Validate key
	// if (!validKeys.has(ctx.r.body.key)) {
	// 	ctx.body = {
	// 		success: false,
	// 		errorCode: errorCode.PERMISSION_DENIED
	// 	};
	// 	return;
	// }

	if (ctx.r.body.key == "verify-xxw") {
		const value = ctx.r.body.value
		const code: string = value.code			//verification code
		console.log("code=",code)
		const realname: string = value.realname	// realname
		console.log("realname=",realname)
		const url = `https://www.chsi.com.cn/xlcx/bg.do?vcode=${code}&srcid=bgcx`
		const res = await axios.get(url)
		console.log("res.data=",res.data)
		const info = xxwParse(res.data)
		//TODO:verification
		if (info == null) {
			console.log("info=null")
			ctx.body = {
				success: false,
				errorCode: errorCode.INVALID_AUTH_CODE
			}
			return
		}

		const user = await (await db).getUserbyId(ctx.params.id);
		let new_user = { ...user, ...info, ...{ realname } }
		new_user["status"] = 1
		await (await db).updateUserById(ctx.params.id, new_user)
		ctx.body = {
			success: true,
			data: info
		}
	}
	else if (ctx.r.body.key == "verify-document") {
		if (!ctx.user)		// only permitted to user logged in
		{
			ctx.body = {
				success: false,
				errorCode: errorCode.PERMISSION_DENIED
			}
		}
		else {
			let files = ctx.r.files.file
			if (!Array.isArray(files)) {
				files = [files]
			}
			let count = 0
			for (const file of files) {
				const path = file.path
				const type = file.type
				const tmp = `doc${count}.${file.name.split('.')[file.name.split('.').length - 1]}`
				console.log("metadata:", path, name, type)
				const content = fs.readFileSync(path)
				const filename: string = `data/verify-document/${ctx.params.id}/${tmp}`
				console.log("save:", filename)
				fs.mkdirSync(`data/verify-document/${ctx.params.id}`, { recursive: true })
				fs.writeFileSync(filename, content)
				count += 1
			}
			ctx.body = {
				success: true
			}
		}
	}
	else if (ctx.r.body.key == "verify")		// only permitted to admin
	{
		if (!ctx.isAdmin) {
			ctx.body = {
				success: false,
				errorCode: errorCode.PERMISSION_DENIED
			}
		}
		else {
			const user = await (await db).getUserbyId(ctx.params.id)
			if (ctx.r.body.value)		// pass
			{
				user["status"] = 1
			}
			else					// reject: return back to pre-verified
			{
				user["status"] = 0
			}
			await (await db).updateUserById(ctx.params.id, user)
			ctx.body = {
				success: true
			}
		}
	}
	else if (ctx.r.body.key == "read-notification") {
		console.log('Read notification')
		const index = ctx.r.body.value
		console.log('index:', index)
		let user = await (await db).getUserbyName(ctx.user.username)
		console.log('user:', user)
		user.notifications[index].read = 1
		console.log('user:', user)
		await (await db).updateUser(ctx.user.username, user)
		ctx.body = {
			success: true
		}
	}
	else if (ctx.r.body.key == "avatar") {
		console.log("avatar")
		let file = null
		try {
			file = ctx.r.files.file
		}
		catch{
			ctx.body = {
				success: false
			}
			return
		}
		finally {
			console.log("file=", file)
			const dir = `data/avatar/${ctx.user.id}`
			try {
				fs.mkdirSync(dir, { recursive: true })
			}
			catch{

			}
			finally {
				const content = fs.readFileSync(file.path)
				// Remove file
				const tmps = fs.readdirSync(dir)
				if (tmps && tmps.length > 0) {
					const removedFile = `${dir}/${tmps[0]}`
					fs.unlinkSync(removedFile)
				}
				fs.writeFileSync(`${dir}/${file.name}`, content)
				ctx.body = {
					success: true
				}
			}
		}
	}
	else {
		const user = await (await db).getUserbyId(ctx.params.id);
		user[ctx.r.body.key] = ctx.r.body.value;
		await (await db).updateUserById(ctx.params.id, user);

		ctx.body = {
			success: true
		};
	}
});

// delete user: only permitted to admin
router.delete('/:id', userInfo, adminValidate, async ctx => {
	if (!ctx.isAdmin) {
		ctx.body =
			{
				success: false,
				errorCode: errorCode.PERMISSION_DENIED
			}
	}
	else {
		await (await db).removeUser(ctx.params.id)
		ctx.body =
			{
				success: true
			}
	}
})

export default router;
